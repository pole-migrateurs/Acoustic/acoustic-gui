/**
 * @author Blivet Laureline
 */

package vues;

import controllers.AnalyseEcouteur;
import models.Base;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;

/**
 * Class pour afficher l'avancement de l'analyse
 */
public class AAnalyse {
    private JPanel panel;                       // le JPanel qui contiendra tous les composants
    private Base base;                          // la base commune à toutes les classe
    private JButton start;                      // bouton utilisé pour lancer l'analyse
    private JButton defaut;                     // bouton utilisé pour remettre les paramêtres par défaut
    private JButton retour;                     // bouton utilisé pour retourner en arrière
    private JProgressBar progressBar;           // la barre de progression ( elle n'est pas afficher dans le projet encore)
    private String path;                        //le chemin vers les vidéos
    private String savePath;                    //le chemin de sauvegarde des vidéos
    private String chemin;                      // l'endroi ou ce trouve le jar
    private JLabel avancement;                  //JLabel contenant les sorties de python
    private JComboBox version;                  // menu déroulant pour choisir la version de l'analyse
    private JButton dossier;                    // bouton utilisé pour choisir le dossier dans lequel on veut sauvegarder l'analyse des vidéos
    private JSlider iou;                        // >=0 x <=1     confiance IOU thres
    private JPanel panelIou;                    // panel dans lequel sera "labelIou" et "iou"
    private JLabel labelIOU;                    // label pour afficher l'iou
    private int[] iouTab;                       // tableau de valeur de l'iou pour pouvoir dessiner les rectangles en fonction de l'iou
    private JPanel rect;                        // panel dans le lequel il y aura les rectangles représentant l'iou
    private JSlider conf_thres;                 // >=0 x <=1     confiance minimum de détéction
    private JLabel labelConf;                   // label pour afficher la confiance minimum de détéction
    private JSlider skip_limit;                 // >=1 x <=5     Nombre maximum d'image vide entre les détéctions
    private JLabel labelSkip;                   // label pour afficher le nombre maximum d'image vide entre les détéctions
    private JSlider min_image;                  // >=2 x <=10    Nombre minimum d'image détécté pour un poisson
    private JLabel labelMin;                    // label pour afficher le nombre minimum d'image détécté pour un poisson
    private JTextField maxCPU;                  // le nombre maximum de cpu à utilisé pour la conversion des vidéos
    private int Mcpu;                           // nombre maximum de cpu à utilisé lors de l'analyse

    private JPanel rond;                        //Jpanel du gif de chargement
    private JLabel r;                           // Le gif
    private JLabel fini;                        // image pour préciser que l'analyse est terminé

    /**
     * Constructeur de la classe
     * @param base la base commune à toutes les classes
     * @param path le chemin vers les vidéos
     */
    public AAnalyse(Base base, String path) {
        this.base = base;
        this.path = path;
        this.panel = new JPanel();
        this.panel.setSize(816, 639);                           // la taille
        this.panel.setBackground(new Color(0, 0, 0, 0));         // la couleur d'arrière plan

        this.avancement = new JLabel();

        this.savePath = "NULL";             // pour eviter les null pointeur exeption

        // récupération de la localisation absolu de l'utilisateur sur l'ordinateur
        chemin = Base.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        chemin = chemin.substring(1, chemin.length()-11);

        ImageIcon icon = new ImageIcon(getClass().getResource("/img/finish.png"));          // image de fin
        fini = new JLabel(icon);
        fini.setBackground(new Color(240,240,240,255));                                 // le fond

        ImageIcon rondIcon = new ImageIcon(getClass().getResource("/img/load.gif"));        // le rond de charement
        r = new JLabel(rondIcon);
        r.setBackground(new Color(240,240,240,255));        // le fond
        this.rond = new JPanel();
        this.rond.setBackground(new Color(240,240,240,255));        // le fond
        this.rond.setLayout(new GridLayout(1,1));               // création du layout
        this.rond.add(r);               // on ajoute le gif au panel
        this.rond.setBorder(BorderFactory.createEmptyBorder(-90, 0, -90, 0));       // marge négative
        r.setVisible(false);            // pour ne pas voir le rond de chargement si on a pas encore lancé l'analyse

        AnalyseEcouteur ecouteur = new AnalyseEcouteur(this.base);          // création de l'écouteur de la classe

        this.start = new JButton(base.getBundle().getString("start"));                      //création du bouton
        this.start.setBackground(Color.LIGHT_GRAY);             // couleur du bouton
        this.start.setBorderPainted(false);                     // boudure invisible
        this.start.setFocusPainted(false);                      // ne pas repeindre au survol
        this.start.addActionListener(ecouteur);                 // ajout de l'écouteur pour faire une action au clic

        this.defaut = new JButton(base.getBundle().getString("par.defaut"));
        this.defaut.setBackground(Color.LIGHT_GRAY);
        this.defaut.setBorderPainted(false);
        this.defaut.setFocusPainted(false);
        this.defaut.addActionListener(ecouteur);

        this.dossier = new JButton(base.getBundle().getString("selectionner.dossier"));
        this.dossier.setBackground(Color.LIGHT_GRAY);
        this.dossier.setBorderPainted(false);
        this.dossier.addActionListener(ecouteur);
        this.dossier.setFocusable(false);

        this.retour= new JButton(base.getBundle().getString("retour"));
        this.retour.setBackground(Color.LIGHT_GRAY);
        this.retour.setBorderPainted(false);
        this.retour.addActionListener(ecouteur);
        this.retour.setFocusable(false);

        String[] nomVersion = { "yoloV5 multiclasse", "yoloV5 monoclasse", "yoloV5 multiclasse.v2", "yoloV5 monoclasse.v2" };            // nom des différentes version
        version = new JComboBox(nomVersion);                                    // crétion du menu déroulant
        version.setSelectedIndex(0);                        // par défaut c'est le yolo multiclasse de séléctionné

        // création de la progressBar (non afficher dans cette classe)
        progressBar = new JProgressBar();
        progressBar.setMinimum(0);
        progressBar.setMaximum(1);
        progressBar.setStringPainted(true);

        this.maxCPU = new JTextField(2);                            // zone de texte modifiable par l'utilisateur
        maxCPU.setBackground(new Color(225, 225, 225, 255));    // le fond
        maxCPU.setText("2");                                                // par defaut le nombre de cpu est 2
        maxCPU.setCaretPosition(maxCPU.getWidth() /2);                      // pour centrer le texte dans le JTextField
        maxCPU.setSelectionColor(new Color(235, 235, 235, 255)); // couleur de l'arière plan du texte quand on le séléctionne
        maxCPU.setSelectedTextColor(new Color(59,159,253));         // couleur du texte quand on le séléctionne
        maxCPU.setHorizontalAlignment(JTextField.CENTER);                   // mettre le texte au milieu
        maxCPU.setBorder(BorderFactory.createEmptyBorder());                // ne pas mettre de bordures
        // KeyListener pour que l'utilisateur rentre que des chiffres dans la zone de texte
        maxCPU.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if ( ((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE)) {
                    e.consume();  // ignorer l'événement
                }
            }
        });


        this.iou = new JSlider(1, 99);                      // création du slider qui va de 1 à 99 (valeur à prendre en pourcentage)
        Hashtable labelTable = new Hashtable();             // création de la table des valeurs
        labelTable.put( 1, new JLabel("0.01") );        // ajout d'une valeur
        labelTable.put(25, new JLabel("0.25") );
        labelTable.put(50, new JLabel("0.50") );
        labelTable.put(75, new JLabel("0.75") );
        labelTable.put(99, new JLabel("0.99") );
        iou.setLabelTable(labelTable);                      // ajout de la table des valeurs au slider
        iou.setValue(45);                                   // valeur par défaut
        iou.setMinorTickSpacing (4);                        // nombre de valeures entre les petits ticks
        iou.setMajorTickSpacing (24);                       // nombre de valeures entre les grands ticks
        iou.setPaintTicks (true);                           // pour afficher les ticks
        iou.setPaintLabels (true);                          // pour afficher la table des valeurs
        // listeneur qui ecoute quand on change la valeur du slider
        iou.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (iou.getValue() < 10) {      // <10 car on doit rajoute un 0 devant la valeur
                    labelIOU.setText("                      :   0.0" + iou.getValue());   // on afficher la nonvelle valeur
                } else {
                    labelIOU.setText("                      :   0." + iou.getValue());    // on affiche la nouvelle valeur
                }
                panelIou.remove(rect);          // on retire le panel des rectangles
                panelIou.remove(labelIOU);      // on retire le label de l'iou
                rect = new JPanel() {
                    protected void paintComponent(Graphics g) {
                        super.paintComponent(g);
                        g.setColor(Color.RED);              // premier rectangle de couleur rouge
                        g.drawRect(10,0, 100,20);    // il ne change pas de place
                        g.setColor(Color.BLUE);             // couleur bleu
                        int x = iouTab[iou.getValue() - 1] + 10 ;       // il prend la bonne valeur d'espacement par raport à l'iou
                        g.drawRect(x, 1, 100, 20);       // redessine le rectangle
                    }
                };
                panelIou.add(rect);             // on remet le panel des rectangles
                panelIou.add(labelIOU);         // on remet le label de l'iou
            }
        });
        this.labelIOU = new JLabel("                      :   0." + iou.getValue());

        // j'ai procédé de la même manière que précédemment
        this.conf_thres = new JSlider(1, 99);
        conf_thres.setLabelTable(labelTable);
        conf_thres.setValue(25);
        conf_thres.setPaintTicks (true);
        conf_thres.setPaintLabels (true);
        conf_thres.setMinorTickSpacing (4);
        conf_thres.setMajorTickSpacing (24);
        conf_thres.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                labelConf.setText(":   0." + conf_thres.getValue());
            }
        });
        this.labelConf = new JLabel(":   0." + conf_thres.getValue());

        this.skip_limit = new JSlider(1, 5);
        Hashtable skipTable = new Hashtable();
        skipTable.put(1, new JLabel("1") );
        skipTable.put(2, new JLabel("2") );
        skipTable.put(3, new JLabel("3") );
        skipTable.put(4, new JLabel("4") );
        skipTable.put(5, new JLabel("5") );
        skip_limit.setLabelTable(skipTable);
        skip_limit.setValue(3);
        skip_limit.setPaintTicks (true);
        skip_limit.setPaintLabels (true);
        skip_limit.setMajorTickSpacing (1);
        skip_limit.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                labelSkip.setText(":   " + skip_limit.getValue());
            }
        });
        this.labelSkip = new JLabel(":   " + skip_limit.getValue());

        this.min_image = new JSlider(2, 10);
        Hashtable minTable = new Hashtable();
        minTable.put(2, new JLabel("2") );
        minTable.put(4, new JLabel("4") );
        minTable.put(6, new JLabel("6") );
        minTable.put(8, new JLabel("8") );
        minTable.put(10, new JLabel("10") );
        min_image.setLabelTable(minTable);
        min_image.setValue(5);
        min_image.setPaintTicks (true);
        min_image.setPaintLabels (true);
        min_image.setMinorTickSpacing (1);
        min_image.setMajorTickSpacing (2);
        min_image.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                labelMin.setText(":   " + min_image.getValue());
            }
        });
        this.labelMin = new JLabel(":   " + min_image.getValue());

        remplirTab();

        this.panelIou = new JPanel();
        panelIou.setLayout(new GridLayout(2,1));
        panelIou.setBackground(new Color(240,240,240,255));

        rect = new JPanel() {
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.setColor(Color.RED);
                g.drawRect(10,0, 100,20);
                g.setColor(Color.BLUE);
                g.drawRect(48, 1, 100, 20);
            }
        };
        panelIou.add(rect);
        panelIou.add(labelIOU);

        placement();
    }

    /**
     * Méthode qui place les composants sur la page, fonctionne de la même façon que la methode homonyme de la classe Accueil
     */
    public void placement() {

        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JLabel titre = new JLabel(base.getBundle().getString("analyse"));
        titre.setBackground(new Color(240,240,240));
        titre.setFocusable(false);
        titre.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 2;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(0, 10, 0, 10);
        c.weightx = 1;
        c.weighty = 1;
        panel.add(titre, c);

        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(0, 10, 10, 10);
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(version, c);

        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(0, 10, 10, 10);
        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 1;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(defaut, c);

        JLabel DossSave = new JLabel(base.getBundle().getString("dossier.de.sauvegarde"));
        DossSave.setFocusable(false);
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(DossSave,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(dossier,c);

        JLabel lIou = new JLabel(base.getBundle().getString("confiance.minimum.de.chevauchement"));
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 3;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(lIou,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 3;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(iou,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 3;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(panelIou,c);

        JLabel conf = new JLabel(base.getBundle().getString("confiance.minimum.de.detection"));
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 4;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(conf,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 4;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(conf_thres,c);

        JPanel panelConf = new JPanel();
        panelConf.setBackground(new Color(240,240,240,255));
        panelConf.add(labelConf);
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 4;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(panelConf,c);

        JLabel skip = new JLabel(base.getBundle().getString("nombre.d.images.vides.entre.les.detections"));
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 5;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(skip,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 5;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(skip_limit,c);

        JPanel panelSkip = new JPanel();
        panelSkip.setBackground(new Color(240,240,240,255));
        panelSkip.add(labelSkip);
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 5;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(panelSkip,c);

        JLabel imm = new JLabel(base.getBundle().getString("nombre.minimum.d.images.detectees.pour.un.poisson"));
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 6;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(imm,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 6;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(min_image,c);

        JPanel panelMin = new JPanel();
        panelMin.setBackground(new Color(240,240,240,255));
        panelMin.add(labelMin);
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 6;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(panelMin,c);

        JLabel jMaxCPU = new JLabel(base.getBundle().getString("max.CPU"));
        jMaxCPU.setFocusable(false);
        jMaxCPU.setBackground(new Color(240,240,240,255));
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 7;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(jMaxCPU,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 7;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(maxCPU,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 3;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 8;
        c.weightx = 0.5;
        c.weighty = 0.5;
        panel.add(avancement, c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 9;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(retour, c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 9;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(rond, c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 2;
        c.gridy = 9;
        c.weightx = 1;
        c.weighty = 1;
        panel.add(start, c);
    }

    /**
     * Ouvre un explorateur de fichier pour pouvoir selectionner un dossier
     */
    public void selecDossSauvegarde() {
        JFileChooser fichierC = new JFileChooser("../");     // on ouvre l'explorateur de fichier
        fichierC.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);   // pour choisir uniquement des dossier
        fichierC.setAcceptAllFileFilterUsed(false);                     // pour nepas accépter ce qui n'est pas un dossier

        int val_retour = fichierC.showOpenDialog(panel);            // valeur de retour (l'utilisateur a appuyé sur "ok" ou "annulé")
        if (val_retour == JFileChooser.APPROVE_OPTION) {            // appuyer sur "ok"
            this.savePath = "" + fichierC.getSelectedFile();     //on sauvegarde le chemin absolu du dossier
        } else {
            JOptionPane.showMessageDialog( panel, base.getBundle().getString("vous.n.avez.pas.selectionnez.de.dossier")," ",JOptionPane.ERROR_MESSAGE);
        }
    }

    public void commencer() {
        r.setVisible(true);                                                         // on affiche le rond de chargement
        analyser(path,savePath,getIOU(),getSkip(),getMin(),getConf(),getMaxCPU());  // on lance l'analyse
    }

    /**
     * renvoie le panel contenant tous les compaosants de la classe
     * @return panel
     */
    public JPanel getPanel() { return this.panel; }

    /**
     * renvoie le bouton pour retourner en arrière
     * @return retour
     */
    public JButton getRetour() { return this.retour; }

    /**
     * renvoie le chemin des vidéos
     * @return path
     */
    public String getPath() { return this.path; }

    /**
     * renvoie le bouton qui sert à demarrer l'analyse
     * @return start
     */
    public JButton getStart() { return this.start; }

    /**
     * renvoie le menu déroulant qui sert à choisir la version de l'analyse
     * @return version
     */
    public JComboBox getVersion() { return this.version; }

    /**
     * renvoie le bouton qui permet de choisir le dossier de sauvegarde des vidéos
     * @return dossier
     */
    public JButton getDossier() { return this.dossier; }

    /**
     * renvoie le chemin vers le dossier de sanvegrde
     * @return savePath
     */
    public String getPathSave() { return this.savePath; }

    /**
     * mets à jours la ligne d'avancelent de l'interface
     * @param line la ligne à mettre à jour
     */
    public void setAvancement( String line) {
        this.avancement.setText(line);
    }

    /**
     * renvoie le nombre maximun de cpu à utiliser pour l'analyse
     * @return maxCPU
     */
    public String getMaxCPU() {
        return maxCPU.getText();
    }

    /**
     * renvoie l'iou à utiliser pour l'analyse
     * @return iou
     */
    public String getIOU() {
        return (iou.getValue()/100.0)+ "";      // %100 car c'est une valeur en pourcentage
    }

    /**
     * renvoie la confiance minimum de détéction
     * @return conf_thres
     */
    public String getConf() {
        return (conf_thres.getValue()/100.0)+ "";
    }

    /**
     * renvoie le nombre maximum d'image vide entre les détéctions
     * @return skip_limit
     */
    public String getSkip() {
        return (skip_limit.getValue())+ "";
    }

    /**
     * renvoie le nombre minimum d'image détécté pour un poisson
     * @return min_image
     */
    public String getMin() {
        return (min_image.getValue())+ "";
    }

    /**
     * renvoie le bouton utilisé pour remettre les paramètres par défaut
     * @return defaut
     */
    public JButton getDefaut() {
        return this.defaut;
    }

    /**
     * Méthode qui remet les sliders, les labels et les rectangles à leur valeur par defaut
     */
    public void parDefaut() {
        iou.setValue(45);
        labelIOU.setText("                      :   0.45");
        conf_thres.setValue(25);
        labelConf.setText(":   0.25");
        skip_limit.setValue(3);
        labelSkip.setText(":   3");
        min_image.setValue(5);
        labelMin.setText(":   5");
        maxCPU.setText("2");

        panelIou.remove(rect);
        panelIou.remove(labelIOU);
        rect = new JPanel() {
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.setColor(Color.RED);
                g.drawRect(10,0, 100,20);
                g.setColor(Color.BLUE);
                g.drawRect(38, 1, 100, 20);
            }
        };
        panelIou.add(rect);
        panelIou.add(labelIOU);
    }

    /**
     * Rempli te tableau des valeurs utiliser pour l'affichage des rectangles
     */
    public void remplirTab() {
        this.iouTab = new int[100];
        iouTab[0] = 98;
        iouTab[1] = 96;
        iouTab[2] = 94;
        iouTab[3] = 92;
        iouTab[4] = 90;
        iouTab[5] = 88;
        iouTab[6] = 86;
        iouTab[7] = 85;
        iouTab[8] = 83;
        iouTab[9] = 81;
        iouTab[10] = 80;
        iouTab[11] = 78;
        iouTab[12] = 77;
        iouTab[13] = 75;
        iouTab[14] = 74;
        iouTab[15] = 72;
        iouTab[16] = 70;
        iouTab[17] = 69;
        iouTab[18] = 67;
        iouTab[19] = 66;
        iouTab[20] = 65;
        iouTab[21] = 63;
        iouTab[22] = 62;
        iouTab[23] = 61;
        iouTab[24] = 59;
        iouTab[25] = 58;
        iouTab[26] = 57;
        iouTab[27] = 56;
        iouTab[28] = 54;
        iouTab[29] = 53;
        iouTab[30] = 52;
        iouTab[31] = 51;
        iouTab[32] = 50;
        iouTab[33] = 49;
        iouTab[34] = 48;
        iouTab[35] = 47;
        iouTab[36] = 46;
        iouTab[37] = 45;
        iouTab[38] = 43;
        iouTab[39] = 42;
        iouTab[40] = 41;
        iouTab[41] = 40;
        iouTab[42] = 39;
        iouTab[43] = 38;
        iouTab[44] = 37;
        iouTab[45] = 36;
        iouTab[46] = 35;
        iouTab[47] = 34;
        iouTab[48] = 33;
        iouTab[49] = 32;
        iouTab[50] = 31;
        iouTab[51] = 31;
        iouTab[52] = 30;
        iouTab[53] = 29;
        iouTab[54] = 28;
        iouTab[55] = 27;
        iouTab[56] = 26;
        iouTab[57] = 26;
        iouTab[58] = 25;
        iouTab[59] = 24;
        iouTab[60] = 23;
        iouTab[61] = 22;
        iouTab[62] = 22;
        iouTab[63] = 21;
        iouTab[64] = 20;
        iouTab[65] = 19;
        iouTab[66] = 19;
        iouTab[67] = 18;
        iouTab[68] = 18;
        iouTab[69] = 17;
        iouTab[70] = 17;
        iouTab[71] = 16;
        iouTab[72] = 15;
        iouTab[73] = 14;
        iouTab[74] = 13;
        iouTab[75] = 12;
        iouTab[76] = 12;
        iouTab[77] = 11;
        iouTab[78] = 10;
        iouTab[79] = 10;
        iouTab[80] = 9;
        iouTab[81] = 9;
        iouTab[82] = 8;
        iouTab[83] = 8;
        iouTab[84] = 7;
        iouTab[85] = 7;
        iouTab[86] = 6;
        iouTab[87] = 6;
        iouTab[88] = 5;
        iouTab[89] = 5;
        iouTab[90] = 4;
        iouTab[91] = 4;
        iouTab[92] = 3;
        iouTab[93] = 3;
        iouTab[94] = 2;
        iouTab[95] = 2;
        iouTab[96] = 2;
        iouTab[97] = 1;
        iouTab[98] = 1;
        iouTab[99] = 1;
    }

    /**
     * Lance le script d'analyse des vidéos
     * @param path le chemin vers les vidéos
     */
    public void analyser(String path, String pathSave, String iou, String skip, String min, String conf, String maxCPU) {

        Mcpu = Integer.parseInt(maxCPU);
        int max = Runtime.getRuntime().availableProcessors();
        if (max == 0) {
            Mcpu = 1;
        } else if (Mcpu > max) {
            Mcpu = max;
        }


        if (base.analyse.getVersion().getSelectedIndex() == 0) {  //yolov5 multiclass
            // lance le code en python
            if (System.getProperty("os.name").equals("Windows 10")) {
                if (savePath.equals("NULL")) {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = ("poissonEnv\\Scripts\\activate.bat && python pythonCode\\yolov5\\pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --weights pythonCode\\yolov5\\multiclasse_best.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave");
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(s);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // display each output line form python script
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                }
                                rond.remove(r);
                                rond.add(fini);
                                rond.revalidate();
                                rond.repaint();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                } else {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = ("poissonEnv\\Scripts\\activate.bat && python pythonCode\\yolov5\\pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --save-path " + pathSave + " --weights pythonCode\\yolov5\\multiclasse_best.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave");
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(s);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                }

            } else {
                if (savePath.equals("NULL")) {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = "source /" + chemin + "poissonEnv/bin/activate && python3 /" + chemin + "pythonCode/yolov5/pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --weights /" + chemin + "pythonCode/yolov5/multiclasse_best.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave";
                                String[] command = new String[]{"bash", "-c", s};
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(command);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();

                } else {

                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = "source /" + chemin + "poissonEnv/bin/activate && python3 /" + chemin + "pythonCode/yolov5/pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --save-path " + pathSave + " --weights /" + chemin + "pythonCode/yolov5/multiclasse_best.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave";
                                String[] command = new String[]{"bash", "-c", s};
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(command);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                }
            }
        } else if (base.analyse.getVersion().getSelectedIndex() == 1) { // yolov5 monoclass
            // lance le code en python
            if (System.getProperty("os.name").equals("Windows 10")) {
                if (pathSave.equals("NULL")) {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = ("poissonEnv\\Scripts\\activate.bat && python pythonCode\\yolov5\\pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --weights pythonCode\\yolov5\\monoclasse_best.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave");
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(s);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();

                } else {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = ("poissonEnv\\Scripts\\activate.bat && python pythonCode\\yolov5\\pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --save-path " + pathSave + " --weights pythonCode\\yolov5\\monoclasse_best.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave");
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(s);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // display each output line form python script
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                }

            } else {
                System.out.println("je suis ici");
                if (pathSave.equals("NULL")) {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = "source /" + chemin + "poissonEnv/bin/activate && python3 /" + chemin + "pythonCode/yolov5/pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --weights /" + chemin + "pythonCode/yolov5/monoclasse_best.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave";
                                String[] command = new String[]{"bash", "-c", s};
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(command);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                } else {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = "source /" + chemin + "poissonEnv/bin/activate && python3 /" + chemin + "pythonCode/yolov5/pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --save-path " + pathSave + " --weights /" + chemin + "pythonCode/yolov5/monoclasse_best.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave";
                                String[] command = new String[]{"bash", "-c", s};
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(command);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                }
            }
        }else if (base.analyse.getVersion().getSelectedIndex() == 2) { // yolov5 multiclasse v2
            // lance le code en python
            if (System.getProperty("os.name").equals("Windows 10")) {
                if (pathSave.equals("NULL")) {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = ("poissonEnv\\Scripts\\activate.bat && python pythonCode\\yolov5\\pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --weights pythonCode\\yolov5\\multiclasse_best_v2.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave");
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(s);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();

                } else {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = ("poissonEnv\\Scripts\\activate.bat && python pythonCode\\yolov5\\pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --save-path " + pathSave + " --weights pythonCode\\yolov5\\multiclasse_best_v2.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave");
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(s);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // display each output line form python script
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                }

            } else {
                System.out.println("je suis ici");
                if (pathSave.equals("NULL")) {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = "source /" + chemin + "poissonEnv/bin/activate && python3 /" + chemin + "pythonCode/yolov5/pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --weights /" + chemin + "pythonCode/yolov5/multiclasse_best_v2.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave";
                                String[] command = new String[]{"bash", "-c", s};
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(command);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                } else {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = "source /" + chemin + "poissonEnv/bin/activate && python3 /" + chemin + "pythonCode/yolov5/pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --save-path " + pathSave + " --weights /" + chemin + "pythonCode/yolov5/multiclasse_best_v2.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave";
                                String[] command = new String[]{"bash", "-c", s};
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(command);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                }
            }
        }else if (base.analyse.getVersion().getSelectedIndex() == 3) { // yolov5 monoclass v2
            // lance le code en python
            if (System.getProperty("os.name").equals("Windows 10")) {
                if (pathSave.equals("NULL")) {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = ("poissonEnv\\Scripts\\activate.bat && python pythonCode\\yolov5\\pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --weights pythonCode\\yolov5\\monoclasse_best_didson.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave");
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(s);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();

                } else {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = ("poissonEnv\\Scripts\\activate.bat && python pythonCode\\yolov5\\pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --save-path " + pathSave + " --weights pythonCode\\yolov5\\monoclasse_best_didson.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave");
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(s);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // display each output line form python script
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                }

            } else {
                System.out.println("je suis ici");
                if (pathSave.equals("NULL")) {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = "source /" + chemin + "poissonEnv/bin/activate && python3 /" + chemin + "pythonCode/yolov5/pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --weights /" + chemin + "pythonCode/yolov5/monoclasse_best_didson.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave";
                                String[] command = new String[]{"bash", "-c", s};
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(command);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                } else {
                    Thread runner = new Thread() {
                        public void run() {
                            try {
                                String s = "source /" + chemin + "poissonEnv/bin/activate && python3 /" + chemin + "pythonCode/yolov5/pipeline_YOLOv5_NOfilter_v3.py --source " + path + " --save-path " + pathSave + " --weights /" + chemin + "pythonCode/yolov5/monoclasse_best_didson.pt --maxCPU " + Mcpu + " --iou-thres " + iou + " --conf-thres " + conf + " --skip_limit " + skip + " --min_image " + min + " --nosave";
                                String[] command = new String[]{"bash", "-c", s};
                                System.out.println(s);
                                Process p = Runtime.getRuntime().exec(command);
                                java.io.InputStream in = p.getInputStream();
                                int i = 0;
                                while ((i = in.read()) != -1) {
                                    Runnable runme = new Runnable() {
                                        public void run() {
                                            BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                                            String line = "";
                                            try {
                                                if ((line = bfr.readLine()) != null) {
                                                    // affiche les lignes de sortie du code en python
                                                    System.out.println(line);
                                                }
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    };
                                    SwingUtilities.invokeLater(runme);
                                }
                                r.setVisible(false);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runner.start();
                }
            }
        }
    }
}