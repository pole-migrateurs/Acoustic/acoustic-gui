/**
 * @author Blivet Laureline
 */

package vues;

import controllers.ImportFEcouteur;
import models.Base;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import static com.sun.javafx.fxml.expression.Expression.add;

public class ImportF {
    private JPanel panel;                   // jpanel de la page
    private Base base;
    private String path;                    // Le chemin vers les vidéos
    private JPanel progress;                // le panel qui contient le gif et la barre de progression

    private JButton suivant;                // bouton suivant qui permet d'aller à la page d'analyse des vidéos
    private JButton retour;                 // bouton retour qui permet de revenir à la page d'acceuil
    private JButton dossier;                // bouton qui permet de lancer l'explorateur de fichier pour choisir le dossier
    private JButton convertir;              // bouton qui lance la convertion des vidéos
    private JProgressBar progressBar;       // la barre de progression
    private JPanel rond;                    // JPanel du Gif
    private JLabel r;                       // Le gif
    private JTextField maxCPU;              // le nombre maximum de cpu à utilisé pour la conversion des vidéos

    private int nbFichier;
    public static int minValue;
    int maxValue;
    int converti;


    /**
     * Constructeur utilisé quand on n'a pas défini le dossier importer
     * @param base
     */
    public ImportF(Base base) {
        this.base = base;
        this.panel = new JPanel();
        this.panel.setSize(816, 639);
        this.panel.setBackground(new Color(240, 240, 240, 255));
        this.progress = new JPanel();
        this.progress.setLayout(new FlowLayout());
        this.progress.setBackground(new Color(240, 240, 240, 255));

        // initialise le temps d'aparation des message lors du survollage des boutons
        ToolTipManager.sharedInstance().setInitialDelay(0);
        ToolTipManager.sharedInstance().setDismissDelay(10000);

        this.path = "NULL";         // initialisation à "NULL" pour eviter les nulls pointers exeptions
        this.nbFichier = 0;

        this.maxCPU = new JTextField(2);
        maxCPU.setBackground(new Color(225, 225, 225, 255));
        maxCPU.setText("2");
        maxCPU.setFont(new Font("Gadugi",Font.BOLD,30));
        maxCPU.setCaretPosition(maxCPU.getWidth() /2);
        maxCPU.setSelectionColor(new Color(235, 235, 235, 255));
        maxCPU.setSelectedTextColor(new Color(59,159,253));
        maxCPU.setHorizontalAlignment(JTextField.CENTER);
        maxCPU.setBorder(BorderFactory.createEmptyBorder());
        maxCPU.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if ( ((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE)) {
                    e.consume();  // ignorer l'événement
                }
            }
        });

        progressBar = new JProgressBar();
        progressBar.setMinimum(0);
        progressBar.setMaximum(1);
        progressBar.setStringPainted(true);

        ImportFEcouteur ecouteur = new ImportFEcouteur(this.base);
        this.suivant = new JButton(base.getBundle().getString("suivant"));
        this.suivant.setBackground(Color.LIGHT_GRAY);
        this.suivant.setBorderPainted(false);
        this.suivant.addActionListener(ecouteur);
        // j'utilise des balises html pour pouvoir faire des sauts de ligne
        this.suivant.setToolTipText(base.getBundle().getString("clique.sur.ce.bouton.pour.allez.a.la.page.de.l.analyse.des.videos"));
        this.retour = new JButton(base.getBundle().getString("retour"));
        this.retour.setBackground(Color.LIGHT_GRAY);
        this.retour.setBorderPainted(false);
        this.retour.addActionListener(ecouteur);
        this.dossier = new JButton(base.getBundle().getString("selectionner.dossier"));
        this.dossier.setBackground(Color.LIGHT_GRAY);
        this.dossier.setBorderPainted(false);
        this.dossier.addActionListener(ecouteur);
        this.dossier.setToolTipText(base.getBundle().getString("clique.sur.ce.bouton.pour.selectionner.le.dossier.ou.se.trouve.les.videos"));
        this.convertir = new JButton(base.getBundle().getString("convertir"));
        this.convertir.setBackground(Color.LIGHT_GRAY);
        this.convertir.setBorderPainted(false);
        this.convertir.addActionListener(ecouteur);
        this.convertir.setToolTipText(base.getBundle().getString("clique.sur.ce.bouton.pour.convertir"));

        placement();
    }


    /**
     * Constructeur utilisé quand on a défini        le dossier importer
     * @param base la Base de la page
     * @param path le chemin du dossier importé
     */
    public ImportF(Base base, String path, String cpu) {
        this.base = base;
        this.panel = new JPanel();
        this.panel.setSize(816, 639);
        this.panel.setBackground(new Color(240, 240, 240, 255));
        this.panel.setFocusable(false);
        this.progress = new JPanel();
        this.progress.setLayout(new FlowLayout(FlowLayout.CENTER));
        this.progress.setBackground(new Color(240,240,240,255));

        this.path = path;
        this.nbFichier = 0;

        this.maxCPU = new JTextField(2);
        maxCPU.setBackground(new Color(225, 225, 225, 255));
        maxCPU.setText(cpu);
        maxCPU.setFont(new Font("Gadugi",Font.BOLD,30));
        maxCPU.setCaretPosition(maxCPU.getWidth() /2);
        maxCPU.setSelectionColor(new Color(235, 235, 235, 255));
        maxCPU.setSelectedTextColor(new Color(59,159,253));
        maxCPU.setHorizontalAlignment(JTextField.CENTER);
        maxCPU.setBorder(BorderFactory.createEmptyBorder());
        maxCPU.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if ( ((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE)) {
                    e.consume();  // ignorer l'événement
                }
            }
        });

        progressBar = new JProgressBar();
        progressBar.setMinimum(0);
        //progressBar.setMaximum(1);
        progressBar.setStringPainted(true);
        this.progressBar.setFocusable(false);

        ImportFEcouteur ecouteur = new ImportFEcouteur(this.base);
        this.suivant = new JButton(base.getBundle().getString("suivant"));
        this.suivant.setBackground(Color.LIGHT_GRAY);
        this.suivant.setBorderPainted(false);
        this.suivant.addActionListener(ecouteur);
        this.suivant.setFocusable(false);
        this.suivant.setToolTipText(base.getBundle().getString("clique.sur.ce.bouton.pour.allez.a.la.page.de.l.analyse.des.videos"));
        this.retour = new JButton(base.getBundle().getString("retour"));
        this.retour.setBackground(Color.LIGHT_GRAY);
        this.retour.setBorderPainted(false);
        this.retour.addActionListener(ecouteur);
        this.retour.setFocusable(false);
        this.dossier = new JButton(base.getBundle().getString("selectionner.dossier"));
        this.dossier.setBackground(Color.LIGHT_GRAY);
        this.dossier.setBorderPainted(false);
        this.dossier.addActionListener(ecouteur);
        this.dossier.setFocusable(false);
        this.dossier.setToolTipText(base.getBundle().getString("clique.sur.ce.bouton.pour.selectionner.le.dossier.ou.se.trouve.les.videos"));
        this.convertir = new JButton(base.getBundle().getString("convertir"));
        this.convertir.setBackground(Color.LIGHT_GRAY);
        this.convertir.setBorderPainted(false);
        this.convertir.addActionListener(ecouteur);
        this.convertir.setFocusable(false);
        this.convertir.setToolTipText(base.getBundle().getString("clique.sur.ce.bouton.pour.convertir"));

        this.progress.setBackground(new Color(240,240,240));
        this.progress.add(progressBar);

        JPanel espace = new JPanel();
        espace.setBorder(BorderFactory.createEmptyBorder(0, 50, 0, 0));
        this.progress.add(espace);

        ImageIcon rondIcon = new ImageIcon(getClass().getResource("/img/load.gif"));
        r = new JLabel(rondIcon);
        r.setBackground(new Color(240,240,240,255));
        rond = new JPanel();
        rond.setSize(rondIcon.getIconWidth(),rondIcon.getIconHeight());
        rond.setBackground(new Color(240,240,240));
        rond.add(r);
        this.progress.add(rond);
        progress.setBorder(BorderFactory.createEmptyBorder(0, 0, -80, 0));
        this.progress.setVisible(false);

        placement();
    }

    /**
     * Méthode qui place les composant sur la page
     */
    public void placement() {

        JPanel site = siteSuivie();
        site.setFocusable(false);
        JPanel fichierImporter = fImporter();
        fichierImporter.setFocusable(false);

        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        // importation fichier 0,0  2
        JLabel titre = new JLabel(base.getBundle().getString("importation.de.fichiers"));
        titre.setFocusable(false);
        titre.setFont(new Font("Comic Sans MS",Font.BOLD,30));
        titre.setBackground(new Color(240,240,240,255));
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 2;  // nombre de colonne
        c.gridx = 0;      // ligne 0
        c.gridy = 0;      // colonne 0
        c.insets = new Insets(10,10,0,10);  //top padding
        c.weightx = 0.1;
        c.weighty = 0.1;
        panel.add(titre,c);

        JLabel fichierBrut = new JLabel(base.getBundle().getString("fichier.brut"));
        fichierBrut.setFocusable(false);
        fichierBrut.setBackground(new Color(240,240,240,255));
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(fichierBrut,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;  // nombre de colonne
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(dossier,c);

        JLabel jMaxCPU = new JLabel(base.getBundle().getString("max.CPU"));
        jMaxCPU.setFocusable(false);
        jMaxCPU.setBackground(new Color(240,240,240,255));
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 3;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(jMaxCPU,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 3;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(maxCPU,c);

        // boutons
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 5;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(retour ,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 5;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(convertir,c);

        // fichier importer
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;
        c.gridheight = 6;  // nombre de ligne
        c.gridx = 3;
        c.gridy = 0;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(fichierImporter,c);

        // temps
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(10,10,5,10);  //top padding
        c.gridwidth = 3;  // nombre de colonne
        c.gridheight = 1;  // nombre de ligne
        c.gridx = 0;
        c.gridy = 6;
        c.weightx = 0.3;
        c.weighty = 0.1;
        //c.ipady = 0;
        panel.add(progress,c);

        // bouton suivant
        c.fill = GridBagConstraints.BOTH;
        c.gridwidth = 1;  // nombre de colonne
        c.gridx = 3;
        c.gridy = 6;
        c.weightx = 0.3;
        c.weighty = 0.1;
        panel.add(suivant,c);

    }

    /**
     * JPanel à droite de la page,
     * permet d'afficher le nom de tous les fichiers importer
     * @return
     */
    public JPanel fImporter() {
        JPanel site = new JPanel(new BorderLayout());
        site.setFocusable(false);
        site.setBackground(new Color(180,180,180,255));
        site.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        JLabel label;


        JLabel fimp = new JLabel(base.getBundle().getString("liste.des.fichiers.importes"));
        fimp.setFocusable(false);
        fimp.setBorder(BorderFactory.createEmptyBorder(00,0,10,0));
        site.add(fimp,BorderLayout.NORTH);

        ArrayList<String> liste = new ArrayList<String>();
        int nbBon = 0;
        JPanel fichier = new JPanel();
        fichier.setFocusable(false);
        if(!path.equals("NULL")) {
            liste = listDirectory(path);
            System.out.println(liste);
            fichier.setLayout(new GridLayout(liste.size(), 1) );
            for (String nom : liste) {
                if (getExtenion(nom).equals(".aris") || getExtenion(nom).equals(".ddf") || getExtenion(nom).equals(".avi") || getExtenion(nom).equals(".mp4") ) {
                    label = new JLabel();
                    label.setText(nom);
                    fichier.add(label);
                    nbBon ++;
                }
                if (getExtenion(nom).equals(".aris") || getExtenion(nom).equals(".ddf") ) {
                    this.nbFichier ++;
                }
            }
        }

        JScrollPane pane = new JScrollPane(fichier);
        pane.setFocusable(false);
        site.add(pane, BorderLayout.CENTER);

        JLabel nombre = new JLabel(base.getBundle().getString("nombre.de.fichiers"));
        nombre.setFocusable(false);

        JPanel fondNombre = new JPanel();
        fondNombre.setFocusable(false);
        fondNombre.setBackground(new Color(255,255,255,255));
        fondNombre.setPreferredSize(new Dimension(60,30));
        JLabel n = new JLabel(String.valueOf(nbBon));  // aficher le nombres de fichier
        n.setFocusable(false);
        fondNombre.add(n);

        JPanel nomb = new JPanel();
        nomb.setFocusable(false);
        nomb.setBackground(new Color(0,0,0,0));
        nomb.add(nombre);
        nomb.add(fondNombre);

        site.add(nomb,BorderLayout.SOUTH);

        return site;
    }

    /**
     * Méthode qui renvoie tous les noms des fichiers dans le dossier importé
     * @param dir le chemin du dossier
     * @return une arrayList de String avec les nom de tous les fichiers
     */
    private ArrayList<String> listDirectory(String dir) {
        ArrayList<String> fichiers = new ArrayList<String>();
        File file = new File(dir);
        File[] files = file.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory() == false) {
                    fichiers.add(files[i].getName());
                }
            }
        }
        return fichiers;
    }

    /**
     * Methode pour rechanger la page
     */
    public void AfficheImportation() {
        base.pageImport(this.path, maxCPU.getText());
    }

    /**
     * Jpanel correspondant au site de suivie (PAS ENCORE FAIT)
     * @return le JPanel
     */
    public JPanel siteSuivie() {
        JPanel imp = new JPanel();
        imp.setFocusable(false);
        imp.setBackground(new Color(0,0,0,0));
        return imp;
    }

    /**
     * renvoie le chemin absolu du dossier sélectionné et NULL si on a pas encore séléctionné de dossier
     * @return le chemin du dossier
     */
    public String getPath() { return path; }

    /**
     * renvoie le JPanel correspondant a cette page
     * @return le JPanel correspondant a cette page
     */
    public JPanel getPanel() { return panel; }


    // getter de JButton utile pour savoir sur quel bouton on va cliquer (utilisé dans la class ImportFEcouteur()

    /**
     * renvoie le bouton retour
     * @return le bouton retour
     */
    public JButton getRetour() { return retour; }

    /**
     * renvoie le bouton suivant
     * @return le bouton suivant
     */
    public JButton getSuivant() { return suivant; }

    /**
     * renvoie la borre de progression
     * @return la progressBar
     */
    public JProgressBar getProgressBar() { return this.progressBar; }

    /**
     * renvoie le bouton convertir
     * @return le bouton convertir
     */
    public JButton getConvertir() { return convertir; }

    /**
     * renvoie le bouton dossier
     * @return le bouton dossier
     */
    public JButton getDossier() { return dossier; }

    /**
     * change le chemin des dossier
     * @param path le nouveau chemin des dossiers
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Ouvre un explorateur de fichier pour pouvoir selectionner un dossier
     */
    public void selecDoss() {
        JFileChooser fichierC = new JFileChooser("../");
        fichierC.setFocusable(false);
        fichierC.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);   // pour choisir uniquement des dossier
        fichierC.setAcceptAllFileFilterUsed(false);

        int val_retour = fichierC.showOpenDialog(panel);
        if (val_retour == JFileChooser.APPROVE_OPTION) {
            //afficher le chemin absolu du fichier
            if ( fichierC.getSelectedFile().isDirectory() ) {
                this.path = "" + fichierC.getSelectedFile();
            } else {
                this.path = "" + fichierC.getCurrentDirectory();
            }

            AfficheImportation();
        } else {
            JOptionPane.showMessageDialog( panel, base.getBundle().getString("vous.n.avez.pas.selectionnez.de.dossier")," ",JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * renvoie l'extention du fichier entrée en paramètre sous forme .format
     * @param nom le nom complet du fichier
     * @return l'extention du fichier
     */
    public String getExtenion(String nom) {
        return nom.substring(nom.lastIndexOf(".")); //récupération de l'extention
    }

    /**
     * Vérifie si on un fichier au bon format (avi ou mp4)
     * @return true si il y au moins 1 vidéo au format avi ou mp4 dans le dossier
     */
    public boolean bonneExt() {
        boolean ret = false;
        ArrayList<String> liste = new ArrayList<String>();

        if(!path.equals("NULL")) {
            liste = listDirectory(path);
        }

        for (String nom : liste) {
            if (getExtenion(nom).equals(".avi") || getExtenion(nom).equals(".mp4")) {
                // c'est bon il y a au moins une bonne vidéo d'un bon format
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Vérifie premièrement qu'on a importé un dossier, ensuite
     * vérifie qu'on a au moins une vidéo au format avi ou mp4 (grace a la méthode bonneExt() )
     * Quand tout est ok lanse le traitement
     */
    public void verifSuivant() {
        if (path.equals("NULL")) {
            JOptionPane.showMessageDialog( panel, base.getBundle().getString("vous.n.avez.pas.selectionnez.de.dossier")," ",JOptionPane.ERROR_MESSAGE);     // affichage message erreur
        } else if (!bonneExt()) {
            JOptionPane.showMessageDialog( panel, base.getBundle().getString("vous.n.avez.pas.converti.les.videos")," ",JOptionPane.ERROR_MESSAGE);
        } else {
            base.pageAnalyse(base.importVue.getPath());
        }
    }

    /**
     * Méthode qui vérifie qu'on a bien importé des vidéos avant d'essayer de les convertir
     * si on a importé des vidéo : les converties.
     */
    public void verifConvertir() {
        if (path.equals("NULL")) {
            JOptionPane.showMessageDialog( panel, base.getBundle().getString("vous.n.avez.pas.selectionnez.de.dossier")," ",JOptionPane.ERROR_MESSAGE);     // affichage message erreur
        } else if (nbFichier == 0) {
            JOptionPane.showMessageDialog( panel, base.getBundle().getString("il.n.y.a.pas.de.video.didson.ou.aris.a.convertir.dans.ce.dossier")," ",JOptionPane.ERROR_MESSAGE);
        } else {
            this.progress.setVisible(true);
            progres();
            base.convert(path);
        }
    }

    /**
     * Méthode qui met à jour la barre de progression
     */
    public void progres() {
        minValue = 0;
        maxValue = 100;
        File file = new File(path);
        converti = 0;

        Thread runner = new Thread() {

            public void run() {
                converti = minValue;
                int fichierTotal = Objects.requireNonNull(file.listFiles()).length;
                while (converti < nbFichier ) {
                    Runnable runme = new Runnable() {
                        public void run() {
                            int progression = (converti*100)/nbFichier;     // valeur en pourcentage
                            if (progression != 100) {
                                progressBar.setValue(progression);
                            } else {
                                progressBar.setValue(99);
                            }
                        }
                    };
                    SwingUtilities.invokeLater(runme);
                    //converti++;
                    if (fichierTotal != Objects.requireNonNull(file.listFiles()).length) {
                        fichierTotal ++;
                        converti ++;
                    }
                    try {
                        Thread.sleep(100);
                    } catch (Exception ex) {
                    }

                }
                int progression = (converti*100)/nbFichier;
                if (progression != 100) {
                    progressBar.setValue(progression);      // met à jour la barre de progression
                } else {
                    progressBar.setValue(99); // met la valeur à 99 car les fichiers sont tous présents mais pas tous converti pour le moment
                }
            }

        };
        runner.start();
    }

    /**
     * Méthode qui met la barre de progression à 100%, cette méthode est appelé à la fin de la convertion
     */
    public void setProgression() {
        progressBar.setValue(100);
        rond.remove(r);
        rond.setForeground(new Color(240, 240, 240, 255));
    }

    /**
     * renvoie le nombre maximum de CPU à utiliser pour la convertion
     * @return maxCPU
     */
    public String getMaxCPU() {
        return maxCPU.getText();
    }
}
