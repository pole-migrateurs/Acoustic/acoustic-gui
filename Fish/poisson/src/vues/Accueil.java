/**
 * @author Blivet Laureline
 */

package vues;

import controllers.AccueilEcouteur;
import models.Base;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

/**
 * Classe d'accueil de l'application
 */
public class Accueil {
    private JPanel panel;                   // le JPanel qui contient tous les composants
    private JLabel titre;                   // le titre en haut a gauche
    private JButton conversion;             // Bouton utiliser pour accéder à la page d'importation
    private JButton creaClip;               // bouton inutile pour le moment qui va servir à axcéder à la page de création de clip
    private JButton langueFr;               // bouton pour mettre la langue en français
    private JButton langueEn;               // bouton pour mettre la langue en anglais
    private JLabel logoLetg;                // le label contenant le logo letg
    private JLabel logoCnrs;                // le label contenant le logo cnrs
    private JLabel logoInrae;               // le label contenant le logo inrae
    private JLabel logoOfb;                 // le label contenant le logo ofb
    private JPanel langue;                  // le panel contenant les deux boutons langueFr et langueEn
    private Base base;                      // la base (c'est la même dans toute les classes)
    private boolean fr;                     // = à true si la langue est français

    /**
     * Contructeur
     * @param base la base de l'application
     * @param fra true si en français false sinon
     */
    public Accueil(Base base, boolean fra) {
        this.base = base;
        this.panel = new JPanel();                                              // Création du JPanel
        this.panel.setSize(816, 639);                               // taille du panel
        this.panel.setBackground(new Color(0, 0, 0, 0));            // couleur d'arrière plan

        // initialise le temps d'aparation des message lors du survollage des boutons
        ToolTipManager.sharedInstance().setInitialDelay(0);
        ToolTipManager.sharedInstance().setDismissDelay(5000);

        AccueilEcouteur ecouteur = new AccueilEcouteur(this.base);              // l'écouteur qui va servir quand on cliquera sur un bouton

        this.fr = fra;
        if ( this.fr ) {    // langue en français
            ImageIcon en = new ImageIcon(getClass().getResource("/img/enOff.png"));             // image anglais sombre
            langueEn = new JButton(en);                                                               // création du bouton avec l'image dedans
            langueEn.setBackground(new Color(240,240,240));                                  // couleur de fond
            langueEn.setBorderPainted(false);                                                         // pour ne pas voir les bordures
            langueEn.addActionListener(ecouteur);                                                     // ajout de l'écouteur au bouton pour pouvoir capter les clics dessus
            ImageIcon fr = new ImageIcon(getClass().getResource("/img/frOn.png"));             // image français claire
            langueFr = new JButton(fr);
            langueFr.setBackground(new Color(240,240,240));
            langueFr.setBorderPainted(false);
            langueFr.addActionListener(ecouteur);
        } else {     //langue en anglais
            ImageIcon en = new ImageIcon(getClass().getResource("/img/enOn.png"));             // image anglais claire
            langueEn = new JButton(en);
            langueEn.setBackground(new Color(240,240,240));
            langueEn.setBorderPainted(false);
            langueEn.addActionListener(ecouteur);
            ImageIcon fr = new ImageIcon(getClass().getResource("/img/frOff.png"));             // image français sombre
            langueFr = new JButton(fr);
            langueFr.setBackground(new Color(240,240,240));
            langueFr.setBorderPainted(false);
            langueFr.addActionListener(ecouteur);
        }
        // on ajoute les boutons au panel langue
        this.langue = new JPanel(new FlowLayout());
        this.langue.add(langueEn);
        this.langue.add(langueFr);

        titre = new JLabel(base.getBundle().getString("analyse.automatique.des.videos.acoustiques"), SwingConstants.CENTER);                // création du titre
        titre.setBackground(Color.LIGHT_GRAY);                              // couleur du fond
        titre.setOpaque(true);                                              // pour voir le fond
        titre.setFont(new Font("Serif", Font.BOLD, 30));          // police d'écriture


        this.conversion = new JButton(base.getBundle().getString("importer"));
        this.conversion.setBackground(Color.LIGHT_GRAY);
        this.conversion.setBorderPainted(false);
        this.conversion.addActionListener(ecouteur);
        this.conversion.setToolTipText(base.getBundle().getString("cliquez.sur.ce.bouton.pour.acceder.a.la.page.pour.importer.des.fichiers"));         // texte visible au survol

        this.creaClip = new JButton(base.getBundle().getString("creation.de.clip"));
        this.creaClip.setBackground(Color.LIGHT_GRAY);
        this.creaClip.setBorderPainted(false);
        this.creaClip.setToolTipText(base.getBundle().getString("ce.bouton.n.est.pas.encore.implemente"));
        this.creaClip.addActionListener(ecouteur);

        // ajout des logo
        ImageIcon letg = new ImageIcon(getClass().getResource("/img/LETG.png"));                                // on récupère l'image dans le dossier ressources
        Image myNewImage = letg.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT);              // on redimentionne l'image
        letg = new ImageIcon(myNewImage);                                                                             // on créer une nouvelle ImageIcon avec l'image redimentionné
        this.logoLetg = new JLabel(letg);                                                                             // on met l'image redimentionné dans le label

        ImageIcon cnrs = new ImageIcon(getClass().getResource("/img/cnrs.png"));
        Image myNewImage2 = cnrs.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT);
        cnrs = new ImageIcon(myNewImage2);
        this.logoCnrs = new JLabel(cnrs);

        ImageIcon inrae = new ImageIcon(getClass().getResource("/img/inrae.png"));
        Image myNewImage3 = inrae.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT);
        inrae = new ImageIcon(myNewImage3);
        this.logoInrae = new JLabel(inrae);

        ImageIcon ofb = new ImageIcon(getClass().getResource("/img/ofb.png"));
        Image myNewImage4 = ofb.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT);
        ofb = new ImageIcon(myNewImage4);
        this.logoOfb = new JLabel(ofb);

        placement();

    }

    /**
     * méthode qui permet de placer tous les composants
     */
    public void placement() {
        // création du layout
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JPanel j1 = new JPanel();
        j1.setBackground(new Color(0, 0, 0, 0));
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 0;                                            //reset to defau
        c.weighty = 0.1;                                        // extra vertical space
        c.weightx = 0.3;                                        // extra horizontal space
        c.gridwidth = 1;                                        // nombre de ligne de large
        c.gridx = 0;                                            // commence à la collonne 0 (la première)
        c.gridy = 0;                                            // commence à la ligne 0 (la première)
        panel.add(j1,c);                                        // on ajoute au panel

        JPanel j2 = new JPanel();
        j2.setBackground(new Color(0, 0, 0, 0));
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 0;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.3;
        panel.add(j2,c);

        JPanel j3 = new JPanel();
        j3.setBackground(new Color(0, 0, 0, 0));
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 0;
        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 0.3;
        panel.add(j3,c);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.insets = new Insets(0,70,0,70);  //top padding
        c.ipady = 5;
        c.ipadx = 5;
        c.weightx = 0.5;
        c.weighty = 0.4;
        c.gridwidth = 5;
        c.gridx =0;
        c.gridy = 1;

        panel.add(titre,c);

        JPanel j4 = new JPanel();
        j4.setBackground(new Color(0, 0, 0, 0));
        c.fill = GridBagConstraints.BOTH;
        c.ipady = 0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 2;
        c.weighty = 0.1;
        panel.add(j4,c);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.PAGE_END;
        c.ipady = 0;
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.insets = new Insets(100,0,10,10);
        c.gridx = 4;
        c.gridwidth = 1;
        c.gridy = 3;
        panel.add(langue, c);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.PAGE_END;
        c.ipady = 0;
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.insets = new Insets(0,0,10,10);
        c.gridx = 4;
        c.gridwidth = 1;
        c.gridy = 4;
        panel.add(conversion, c);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.PAGE_END;
        c.ipady = 0;
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.insets = new Insets(0,0,20,10);
        c.gridx = 4;
        c.gridwidth = 1;
        c.gridy = 5;
        panel.add(creaClip, c);

        c.fill = GridBagConstraints.BOTH;
        c.ipady = 0;
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.gridx = 0;
        c.gridwidth = 1;
        c.gridheight = 3;
        c.gridy = 4;
        panel.add(logoLetg, c);

        c.fill = GridBagConstraints.BOTH;
        c.ipady = 0;
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.gridx = 1;
        c.gridwidth = 1;
        c.gridheight = 3;
        c.gridy = 4;
        panel.add(logoCnrs, c);

        c.fill = GridBagConstraints.BOTH;
        c.ipady = 0;
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.gridx = 2;
        c.gridwidth = 1;
        c.gridheight = 3;
        c.gridy = 4;
        panel.add(logoInrae, c);

        c.fill = GridBagConstraints.BOTH;
        c.ipady = 0;
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.gridx = 3;
        c.gridwidth = 1;
        c.gridheight = 3;
        c.gridy = 4;
        panel.add(logoOfb, c);
    }

    /**
     * change les bouton quand l'utilisateur clique sur français
     */
    public void setLangueFr() {
        ImageIcon enOff = new ImageIcon(getClass().getResource("/img/enOff.png"));          // image drapeau anglais foncé
        langueEn.setIcon(enOff);
        ImageIcon frOn = new ImageIcon(getClass().getResource("/img/frOn.png"));            // image drapeau français
        langueFr.setIcon(frOn);
        fr = true;                              // true car langue en fraçais
        ResourceBundle bundle = ResourceBundle.getBundle("domaine.properties.langue_fr");           // on mets à jour le bundle pour que le langue sois en français
        setBaseBundle(bundle);
    }

    /**
     * change les bouton quand l'utilisateur clique sur anglais
     */
    public void setLangueEn() {                    // si en anglais
        ImageIcon enOn = new ImageIcon(getClass().getResource("/img/enOn.png"));                // image drapeau anglais
        langueEn.setIcon(enOn);
        ImageIcon frOff = new ImageIcon(getClass().getResource("/img/frOff.png"));              // image drapeau français foncé
        langueFr.setIcon(frOff);
        fr = false;                             // false car langue en anglais
        ResourceBundle bundle = ResourceBundle.getBundle("domaine.properties.langue_en");             // on mets à jour le bundle pour que le langue sois en anglais
        setBaseBundle(bundle);
    }

    /**
     * renvoie le JPanel contenant tout les composants de la page
     * @return le JPanel
     */
    public JPanel getPanel() {
        return panel;
    }

    /**
     * renvoie le bouton de convertion
     * @return le JButton convertion
     */
    public JButton getConvertion() {
        return conversion;
    }

    /**
     * renvoie le bouton de création de clip
     * @return le JButton creaclip
     */
    public JButton getClip() {
        return creaClip;
    }

    /**
     * renvoie le bouton du drapeau français
     * @return le JButton langueFr
     */
    public JButton getLangueFr() { return langueFr; }

    /**
     * renvoie le bouton du dranpeau anglais
     * @return le JButton langueEn
     */
    public JButton getLangueEn() { return langueEn; }

    /**
     * méthode qui permet de mettre à jour le bundle
     * @param b le nouveau bundle
     */
    public void setBaseBundle(ResourceBundle b) {
        base.setBundle(b);
        base.pageAccueil(this.fr);
    }

}
