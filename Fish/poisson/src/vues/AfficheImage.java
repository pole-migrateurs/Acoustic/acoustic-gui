package vues;

import controllers.ImportFEcouteur;
import models.Base;

import javax.swing.*;
import java.awt.*;
import java.io.File;

public class AfficheImage {
    private JPanel panel;
    private Base base;
    String imagePath;


    public AfficheImage(Base base, String imagePath) {
        this.base = base;
        this.panel = new JPanel();
        this.panel.setSize(816, 639);
        this.panel.setBackground(new Color(0, 0, 0, 0));

        this.imagePath = "" + imagePath;
        if (this.imagePath.equals("")) {
            this.imagePath = ".";
        }


        ImportFEcouteur ecouteur = new ImportFEcouteur(this.base);



        // C:\Users\laubl\OneDrive\Bureau\IUT\UBSlogoDet.png
        // C:\Users\laubl\OneDrive\Bureau\STAGE\image_test\01220.tiff

       /* File file = new File("C:\\Users\\laubl\\OneDrive\\Bureau\\STAGE\\image_test\\01220.tiff");
        BufferedImage bufferedImage = new BufferedImage();
        ImageIO.write(bufferedImage, "TIFF", file);

        ImageIcon icon = new ImageIcon("");
        try {
            BufferedImage img = ImageIO.read(new File("C:\\Users\\laubl\\OneDrive\\Bureau\\STAGE\\image_test\\01220.tif"));
            JLabel imag = new JLabel(new ImageIcon(img));
            panel.add(imag);
        } catch (IOException e) {}*/


//        ImageIcon icon = new ImageIcon(new ImageIcon("C:\\Users\\laubl\\OneDrive\\Bureau\\STAGE\\image_test_png\\01220.png").getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
//        JLabel imag = new JLabel(icon);
//        panel.add(imag);

        System.out.println("path : " + this.imagePath);
        File dir  = new File(this.imagePath);
        File[] liste = dir.listFiles();

        if (liste != null) {
            if (!imagePath.equals(".")) {
                for(File item : liste){
                    if(item.isFile())
                    {
                        ImageIcon icone = new ImageIcon(new ImageIcon(imagePath + "\\" + item.getName()).getImage().getScaledInstance(100, 70, Image.SCALE_DEFAULT));
                        System.out.println(imagePath + "\\" + item.getName());
                        JLabel image = new JLabel(icone);
                        panel.add(image);
                    }
                }
            }
        }


    }

    public JPanel getPanel() {
        return panel;
    }

    public void setImagePath(String path) {
        this.imagePath = path;
    }




}
