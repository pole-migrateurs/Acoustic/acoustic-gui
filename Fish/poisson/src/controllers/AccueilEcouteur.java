/**
 * @author Blivet Laureline
 */

package controllers;

import models.Base;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AccueilEcouteur implements ActionListener {

    Base base;

    /**
     * classe qui permet d'écouter les boutons de la classe Accueil
     * @param base la base de l'application
     */
    public AccueilEcouteur(Base base){
        this.base = base;
    }

    /**
     * Méthode qui choisis ce qui doit faire l'interface en fonction du bouton séléctionné
     * @param e l'action exercé
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == base.accueil.getConvertion()) {
            base.pageImport();
        } else if (e.getSource() == base.accueil.getClip()) {
        } else if (e.getSource() == base.accueil.getLangueFr()) {
            base.accueil.setLangueFr();
        } else if (e.getSource() == base.accueil.getLangueEn()) {
            base.accueil.setLangueEn();
        }
    }
}
