/**
 * @author Blivet Laureline
 */

package controllers;

import models.Base;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AnalyseEcouteur implements ActionListener {

    Base base;

    /**
     * classe qui permet d'écouter les boutons de la classe Analyse
     * @param base la base de l'application
     */
    public AnalyseEcouteur(Base base){
        this.base = base;
    }

    /**
     * Méthode qui choisis ce qui doit faire l'interface en fonction du bouton séléctionné
     * @param e l'action exercé
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == base.analyse.getStart()) {
            base.analyse.commencer();
        } else if (e.getSource() == base.analyse.getDossier()) {
            base.analyse.selecDossSauvegarde();
        } else if (e.getSource() == base.analyse.getRetour()) {
            base.pageImport(base.analyse.getPath(),base.importVue.getMaxCPU());
        } else if (e.getSource() == base.analyse.getDefaut()) {
            base.analyse.parDefaut();
        }
    }
}
