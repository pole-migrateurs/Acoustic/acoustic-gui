/**
 * @author Blivet Laureline
 */

package controllers;

import models.Base;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ImportFEcouteur implements ActionListener {

    Base base;

    /**
     * classe qui permet d'écouter les boutons de la classe ImportF
     * @param base la base de l'application
     */
    public ImportFEcouteur(Base base){
        this.base = base;
    }

    /**
     * Méthode qui choisis ce qui doit faire l'interface en fonction du bouton séléctionné
     * @param e l'action exercé
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == base.importVue.getDossier()) {
            base.importVue.selecDoss();
        }else if (e.getSource() == base.importVue.getSuivant()) {
            base.importVue.verifSuivant();
        } else if (e.getSource() == base.importVue.getRetour()) {
            base.pageAccueil();
        } else if (e.getSource() == base.importVue.getConvertir()) {
            base.importVue.verifConvertir();
        }
    }
}
