/**
 * @author Blivet Laureline
 */

package models;

import vues.*;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ResourceBundle;

/**
 * Classe de base de notre application, on y trouvera tout les panels à afficher
 */
public class Base {

    private JFrame frame;               // la frame de l'application
    private JPanel current;             // le JPanel afficher, il change en fonction de la page
    public Accueil accueil;             // la classe accueil qui nous renvoie le JPanel a afficher
    public ImportF importVue;           // la classe d'importation de fichier qui nous renvoie le JPanel a afficher
    public AAnalyse analyse;            // la classe d'analyse qui nous renvoie le JPanel a afficher
    private String chemin;              // le chemin absolu de où on est
    public String version;              // la version de python des utilisateurs
    private int cpu;                    // le nombre de cpu
    private ResourceBundle bundle;      // le bundle utilisser pour la traduction de l'application

    /**
     * Constructeur de Base
     */
    public Base() {
        this.frame = new JFrame("Poisson");                                     // création de la frame avec comme titre "poisson"
        this.frame.setSize(816, 639);                                   // taille de la frame
        this.frame.setBackground(new Color(240,240,240));                   // le fond
        this.current = new JPanel();                                                // création du JPanel qui contiendra les différentes pages
        this.current.setSize(816,639);                                  // taille du panel
        this.frame.add(this.current);                                               // on ajoute le panel à la frame
        this.frame.setVisible(true);                                                // on rend la frame visible
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();                // on récupère la taille de l'écran pour aficher l'application au milieu quand on l'ouvre
        this.frame.setLocation(dim.width/2-this.frame.getSize().width/2, dim.height/2-this.frame.getSize().height/2);           // affiche l'application au milieu de l'écran
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);                  // fermer l'application quand on clique sur la croix
        this.frame.setResizable(false);                                             //empeche la redimention de la fenêtre

        bundle = ResourceBundle.getBundle("domaine.properties.langue");             // Initialisation du bunble ici par defaut la langue est français

        ImageIcon logo = new ImageIcon(getClass().getResource("/img/poisson.png"));
        this.frame.setIconImage(logo.getImage());                                               // permet de mettre une icone à l'application

        chemin = Base.class.getProtectionDomain().getCodeSource().getLocation().getFile();      // récupération du nom entier avec le chemin du fichier éxécuté
        chemin = chemin.substring(1, chemin.length()-11);                                       // on enlève les 11 dernier caractères "poisson.jar" pour récupérer que le chemin

        pageAccueil();
    }

    /**
     * Affiche la page d'accueil
     */
    public void pageAccueil() {
        this.accueil = new Accueil(this, true);     // création de la page Accueil
        frame.remove(this.current);                         // on enlève le précedent panel
        this.current = accueil.getPanel();                  // on met le panel de la page d'accueil dans this.current
        frame.add(this.current);                            // on ajoute à la frame
        frame.revalidate();                                 // on valide
        frame.repaint();                                    // et on repeint
    }

    /**
     * Affiche la page d'accueil
     */
    public void pageAccueil(boolean fra) {
        this.accueil = new Accueil(this,fra);           // création de la page Accueil
        frame.remove(this.current);                         // on enlève le précedent panel
        this.current = accueil.getPanel();                  // on met le panel de la page d'accueil dans this.current
        frame.add(this.current);                            // on ajoute à la frame
        frame.revalidate();                                 // on valide
        frame.repaint();                                    // et on repeint
    }

    /**
     * Affiche la page pour importer les vidéos
     */
    public void pageImport() {
        this.importVue = new ImportF(this);            // création de la page importVue
        frame.remove(this.current);                         // on enlève le précedent panel
        this.current = importVue.getPanel();                // on met le panel de la page d'accueil dans this.current
        frame.add(this.current);                            // on ajoute à la frame
        frame.revalidate();                                 // on valide
        frame.repaint();                                    // et on repeint
    }

    /**
     * Affiche la page pour importer les vidéos quand on a dit où elles sont
     * @param path le chemin des vidéos
     */
    public void pageImport(String path, String cpu) {
        this.importVue = new ImportF(this, path, cpu); // création de la page importVue
        frame.remove(this.current);                         // on enlève le précedent panel
        this.current = importVue.getPanel();                // on met le panel de la page d'accueil dans this.current
        frame.add(this.current);                            // on ajoute à la frame
        frame.revalidate();                                 // on valide
        frame.repaint();                                    // et on repeint
    }

    /**
     * Affiche la page d'analyse des vidéos
     * @param path le chemin vers les vidéos
     */
    public void pageAnalyse(String path) {
        this.analyse = new AAnalyse(this,path);        // création de la page analyse
        frame.remove(this.current);                         // on enlève le précedent panel
        this.current = analyse.getPanel();                  // on met le panel de la page d'accueil dans this.current
        frame.add(this.current);                            // on ajoute à la frame
        frame.revalidate();                                 // on valide
        frame.repaint();                                    // et on repeint
    }

    /**
     * Renvoie la frame de la page
     * @return la frame de la page
     */
    public JFrame getFrame() {
        return this.frame;
    }

    /**
     * Renvoie le panel afficher à l'écran
     * @return le panel affiché à l'ecran
     */
    public JPanel getCurrent() { return this.current; }

    /**
     * retroune la langue
     * @return le bundle
     */
    public ResourceBundle getBundle() { return this.bundle; }

    /**
     * change la langue de l'interface
     * @param b le bundle de la langue
     */
    public void setBundle(ResourceBundle b) {
        this.bundle = b;
    }

    /**
     * Convertie les vidéos didson et aris en avi (le code est en python ici on le lance juste)
     * @param path le chemin vers les vidéos
     */
    public void convert(String path) {
        // lance le code en python
        this.cpu = Integer.parseInt(importVue.getMaxCPU());         // récupère le nombre de cpu rentré par l'utilisateur
        int max = Runtime.getRuntime().availableProcessors();       // récupère le nombre maximum de cpu disponible sur l'ordinateur
        if ( cpu > max) {                                           // si le nombre de cpu rentré par l'utilisateur est superieur au nombre maximum de cpu disponible sur l'ordinateur
            cpu = max;                                              // on prend le nombre maximum de cpu disponible sur l'ordinateur
        }
        if (System.getProperty("os.name").equals("Windows 10")) {   // si on est sous window 10
            Thread runner = new Thread() {
                public void run() {
                    try {
                        Process p = Runtime.getRuntime().exec("poissonEnv\\Scripts\\activate.bat && python pythonCode\\conversion\\sonar_converter.py --path " + path + " --maxCPU " + cpu);       // commande pour lancer le code python
                        java.io.InputStream in = p.getInputStream();
                        int i = 0;
                        while ((i = in.read()) != -1) { }       // pour soivoir si le programe tourne encore
                        importVue.setProgression();
                    } catch (IOException e) {
                        e.printStackTrace();                    // en cas d'erreur
                    }
                }
            };
            runner.start();         // on lance le thread
        } else {                    // on est pas sous window 10
            Thread runner = new Thread() {
                public void run() {
                    try {
                        String s = "source /" + chemin + "poissonEnv/bin/activate && python3 /" + chemin + "pythonCode/conversion/sonar_converter.py --path " + path + " --maxCPU " + cpu;
                        String[] command = new String[]{"bash", "-c", s};
                        Process p = Runtime.getRuntime().exec(command);
                        java.io.InputStream in = p.getInputStream();
                        int i = 0;
                        while ((i = in.read()) != -1) {}
                        importVue.setProgression();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            runner.start();
        }
    }
}
