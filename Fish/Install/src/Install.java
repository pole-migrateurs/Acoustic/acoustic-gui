/**
 * @author Blivet Laureline
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLOutput;

/**
 * Classe d'accueil de l'application
 */
public class Install {
    private JFrame frame;
    private JPanel panel;
    private JLabel titre;
    private JButton annule;
    private JButton ok;
    private JPanel rond;
    private JLabel r;

    /**
     * Contructeur
     */
    public Install() {
        this.frame = new JFrame("Installation");                                            //création de la JFrame avec comme titre Installation
        this.frame.setSize(500,300);                                                // taille de la JFrame

        ImageIcon logo = new ImageIcon(getClass().getResource("/img/poisson.png"));
        this.frame.setIconImage(logo.getImage());                                               // ajout d'une icone à la fenêtre

        this.panel = new JPanel();                                                              // création d'un panel qui va contenir le differents composants
        this.panel.setSize(500, 300);                                               // taille du panel

        this.titre = new JLabel("Installation");                                            // zone de texte
        titre.setFont(new Font("Gadugi", Font.BOLD, 20));                             // changement de la police et de la taille du label

        rond = new JPanel();
        ImageIcon rondIcon = new ImageIcon(getClass().getResource("/img/load.gif"));      // rond de chargement au format gif
        r = new JLabel(rondIcon);                                                               // label dans lequel on met le gif
        r.setBackground(Color.WHITE);                                                           // couleur de l'arrière plan
        rond.setBackground(Color.WHITE);
        rond.setLayout(new GridLayout(1,1));
        rond.add(r);


        annule = new JButton("Annuler");                                                    // bouton pour annuler l'installation
        annule.setFont(new Font("Gadugi", Font.BOLD, 15));
        annule.setBackground(new Color(200,200,200));
        annule.setBorderPainted(false);
        annule.setFocusable(false);
        annule.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        ok = new JButton("   Ok   ");
        ok.setFont(new Font("Gadugi", Font.BOLD, 15));
        ok.setBackground(new Color(200,200,200));
        ok.setBorderPainted(false);
        ok.setFocusable(false);
        ok.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        ok.setEnabled(false);

        placement();
        this.frame.add(this.panel);
        this.frame.setVisible(true);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation(dim.width/2-this.frame.getSize().width/2, dim.height/2-this.frame.getSize().height/2);           // affiche l'application au milieu de l'écran
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setBackground(Color.WHITE);


        installation();


    }

    public void placement() {
        panel.setLayout(new GridBagLayout());
        panel.setBackground(Color.WHITE);
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.BOTH;
        c.ipady = 0;       //reset to default
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.gridwidth = 3;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(10,10,0,10);
        this.panel.add(this.titre,c);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.CENTER;
        c.ipady = 0;       //reset to default
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 1;
        this.panel.add(rond,c);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.PAGE_END;
        c.ipady = 0;       //reset to default
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.insets = new Insets(0,10,10,0);  //top padding
        c.gridx = 0;       //aligned with button 2
        c.gridwidth = 1;   //2 columns wide
        c.gridy = 2;       //fivfh row
        this.panel.add(annule,c);

        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.PAGE_END;
        c.ipady = 0;       //reset to default
        c.weightx = 0.2;
        c.weighty = 0.1;
        c.insets = new Insets(0,0,10,10);  //top padding
        c.gridx = 2;       //aligned with button 2
        c.gridwidth = 1;   //2 columns wide
        c.gridy = 2;       //fivfh row
        this.panel.add(ok,c);

    }

    public void installation() {

        String path = Install.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        path = path.substring(1, path.length()-11);
        System.out.println(path);
        if (System.getProperty("os.name").equals("Windows 10")) {
            System.out.println("Vous etes sous Windows");

            try {
                Process p = Runtime.getRuntime().exec("python -m venv " + path + "poissonEnv");
                java.io.InputStream in = p.getInputStream();
                int i = 0;
                while((i = in.read())!= -1) {
                    BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line = null;
                    try {
                        if ((line = bfr.readLine()) != null) {
                            // display each output line form python script
                            System.out.println(line);
                            titre.setText(line);
                            frame.revalidate();
                            frame.repaint();
                        }
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                Process p = Runtime.getRuntime().exec(path + "poissonEnv/Scripts/activate.bat && pip install -r pythonCode/requirements.txt");
                java.io.InputStream in = p.getInputStream();
                int i = 0;
                while((i = in.read())!= -1) {
                    BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line = "";
                    try {
                        if ((line = bfr.readLine()) != null) {
                            // display each output line form python script
                            System.out.println(line);
                            titre.setText(line);
                            frame.revalidate();
                            frame.repaint();
                        }
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            } catch (IOException e ) {
                e.printStackTrace();
            }
            System.out.println("C est finis !");
            ok.setEnabled(true);
            titre.setText("Intallation réussie !");
            rond.remove(r);
            ImageIcon rondIcon = new ImageIcon(getClass().getResource("/img/valide.png"));
            r = new JLabel(rondIcon);
            r.setBackground(Color.WHITE);
            rond.setBackground(Color.WHITE);
            rond.setLayout(new GridLayout(1,1));
            rond.add(r);
            frame.revalidate();
            frame.repaint();
        } else if (System.getProperty("os.name").equals("Linux")) {
            System.out.println("Vous etes sous Linux");
            try {
                String s =  "python3 -m venv /" + path + "/poissonEnv";
                String[] command = new String[]{"bash", "-c", s};
                Process p = Runtime.getRuntime().exec(command);
                java.io.InputStream in = p.getInputStream();
                int i = 0;
                while((i = in.read())!= -1) {
                    BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line = "";
                    try {
                        if ((line = bfr.readLine()) != null) {
                            // display each output line form python script
                            System.out.println(line);
                            titre.setText(line);
                            frame.revalidate();
                            frame.repaint();
                        }
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                String s =  "source /" + path + "poissonEnv/bin/activate && pip3 install -r /" + path + "pythonCode/requirements.txt";
                String[] command = new String[]{"bash", "-c", s};
                System.out.println(s);
                Process p = Runtime.getRuntime().exec(command);
                java.io.InputStream in = p.getInputStream();
                int i = 0;
                while((i = in.read())!= -1) {
                    BufferedReader bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line = "";
                    try {
                        if ((line = bfr.readLine()) != null) {
                            // display each output line form python script
                            System.out.println(line);
                            titre.setText(line);
                            frame.revalidate();
                            frame.repaint();
                        }
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("C est finis !");
            ok.setEnabled(true);
            titre.setText("Intallation réussie !");
            rond.remove(r);
            ImageIcon rondIcon = new ImageIcon(getClass().getResource("/img/valide.png"));
            r = new JLabel(rondIcon);
            r.setBackground(Color.WHITE);
            rond.setBackground(Color.WHITE);
            rond.setLayout(new GridLayout(1,1));
            rond.add(r);
            frame.revalidate();
            frame.repaint();
        }
    }
}
