#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 14:53:20 2021

@author: marc
"""

import numpy as np
import pandas as pd
import os
import glob

from dataclasses import dataclass, field
from collections import Counter
import csv
import datetime
import shutil
import random
import cv2

@dataclass
class Poisson:
    df_images: pd.DataFrame = field(repr=False,
                                    default=pd.DataFrame(columns=["espece",
                                                                  "x", "y",
                                                                  "width",
                                                                  "height",
                                                                  "name"])
                                    )
    nb_images: int = field(init=False, default=0)
    img_path: str = field(repr=False, default='')
    espece: list = field(init=False, default_factory=list)
    proba: list = field(init=False, default_factory=list)
    time_code: str = field(init=False, default='')
    name: str = field(init=False, repr=False, default='')
    reel_time: str = field(repr=False, default='')
    frame: int = field(default=0)
    direction: int = field(init=False, repr=False, default=0)
    fps: int = field(repr=False, default=0)
    date: date = field(init=False, repr=False, default='')


    def add_image(self, label):
        self.df_images = self.df_images.append(label)

    def next_frame(self, label_2, seuil = 0.3, pred_frame=0, skip_limit=3):
        detection1 = self.df_images.iloc[-1]
        detection_suivante = np.zeros(len(label_2))
        for k,l in enumerate(label_2.values):
            detection_suivante[k] = get_iou(xywh2xyxy(detection1[1:5]), xywh2xyxy(l[1:5]))

        argmax = detection_suivante.argmax()
        cond3 = detection_suivante[argmax] > seuil

        frame = int(label_2.iloc[0]['name'].split('_')[-1])
        cond2 = pred_frame + skip_limit + 1 >= frame

        if cond3 and cond2:
            df = pd.DataFrame(label_2.iloc[argmax]).T
            self.add_image(df)
            self.nb_images += 1
            label_2.drop(argmax, inplace=True)

        return cond3 and cond2, label_2, frame

    def detection(self, list_image, skip_limit=3):
        nb_skip = 0
        list_skip = []
        for i,j in enumerate(list_image):
            if isinstance(j, str):
                label = pd.read_csv(j, sep=' ', header=None,
                                     names=["espece", "x", "y", "width", "height"])
                label['name'] = [os.path.basename(j)[:-4]] * len(label)

            else:
                label = j

            label.set_index(np.arange(len(label)), inplace=True)

            if i == 0:
                self.add_image(label.iloc[i])
                pred_frame = int(label.iloc[0]['name'].split('_')[-1])
                if len(label) > 1:
                    label.drop(i, inplace=True)
                    list_skip.append(label)
                continue


            suivante_ok, label_2, pred_frame = self.next_frame(label, seuil=0.2/(nb_skip+1),
                                                   pred_frame=pred_frame,
                                                   skip_limit=skip_limit)

            if suivante_ok:
                nb_skip = 0
                if len(label) > 1:
                    list_skip.append(label_2)

            else:
                nb_skip += 1
                list_skip.append(label_2)
                if nb_skip == skip_limit+1:
                    break

        return list_skip, i


    def def_espece(self):
        vote =  Counter(self.df_images['espece'])
        top_two = vote.most_common(3)
        list_espece = {0:'ALO',
                       1:'ANG',
                       2:'SAT',
                       3:'SIL',
                       4:'LPM',
                       5:'AUTRE'}
        for i in range(len(top_two)):
            self.espece.append(list_espece[top_two[i][0]])
            self.proba.append(top_two[i][1]/(self.nb_images+1))
        self.espece.extend(['NULL','NULL'])
        self.proba.extend([0,0])


    def make_name(self):
        self.name = self.df_images.iloc[0]['name']
        split_name = self.name.split('_')
        self.frame = int(split_name[-1])

        # self.fps = 8 #temporaire

        try :
            self.time_code = datetime.timedelta(seconds=int(self.frame/self.fps))
            heure_brute = split_name[-3]
            start = datetime.timedelta(hours = int(heure_brute[:2]),
                                 minutes = int(heure_brute[2:4]),
                                 seconds = int(heure_brute[4:6]))
            self.reel_time = start + self.time_code
            self.date = split_name[1]


        except (ValueError, IndexError):
            print('Mauvais format de nom de vidéo')


    def out_list(self):
        return [str(self.name), str(self.date),  str(self.reel_time), str(self.time_code),
                self.frame, self.nb_images,
                self.espece[0], self.espece[1], self.espece[2],
                f'{self.proba[0]:.2f}', f'{self.proba[1]:.2f}',
                f'{self.proba[2]:.2f}', f'{self.direction:.2f}'
                ]


    def calcul_direction(self):
        direction = []
        for i,j in enumerate(self.df_images.values):
            if i == 0:
                pre = j
            deplacement = j[1] - pre[1]
            direction.append(deplacement)
            pre = j
        self.direction = np.mean(direction)*100

    def save_position(self, path):
        with open(os.path.join(path, str(self.name) + '.csv'), 'w') as file:
            writer = csv.writer(file)
            for i in self.df_images.values:
                writer.writerow(i)


def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = [0,0,0,0]
    y[0] = x[0] - x[2] / 2  # top left x
    y[1] = x[1] - x[3] / 2  # top left y
    y[2] = x[0] + x[2] / 2  # bottom right x
    y[3] = x[1] + x[3] / 2  # bottom right y
    xy = ['x1','y1','x2','y2']
    res = {i:j for i,j in zip(xy, y)}
    return res

def xywhn2xyxy1(x, w=640, h=640, padw=0, padh=0):
    # Convert nx4 boxes from [x, y, w, h] normalized to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    x1 = w * (x[1] - x[3] / 2) + padw  # top left x
    y1 = h * (x[2] - x[4] / 2) + padh  # top left y
    x2 = w * (x[1] + x[3] / 2) + padw  # bottom right x
    y2 = h * (x[2] + x[4] / 2) + padh  # bottom right y
    return int(x1), int(y1), int(x2), int(y2)

def get_iou(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]
    """
    assert bb1['x1'] < bb1['x2']
    assert bb1['y1'] < bb1['y2']
    assert bb2['x1'] < bb2['x2']
    assert bb2['y1'] < bb2['y2']

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1['x1'], bb2['x1'])
    y_top = max(bb1['y1'], bb2['y1'])
    x_right = min(bb1['x2'], bb2['x2'])
    y_bottom = min(bb1['y2'], bb2['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
    bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou


def analyse_1_video(dir_path, dir_image = '', video=True,
                    skip_limit=3, min_image=5,
                    save_name=False, fps=10):


    list_image = glob.glob(str(dir_path) + '/*.txt')
    print(str(dir_path))
    list_image.sort()

    if video:

        dict_image = {}
        for i in list_image:
            j = int(os.path.basename(i)[:-4].split('_')[-1])
            dict_image[j] = i

        list_sort = sorted(dict_image.items())
        list_image = []
        for i in list_sort:
            list_image.append(i[1])


    list_individus = []
    new_list = True

    if list_image:
        while new_list:

            if isinstance(list_image[0], str):
                label = pd.read_csv(list_image[0], sep=' ', header=None,
                                  names=["espece", "x", "y", "width", "height"])
            else:
                label=list_image[0]

            new_list = []
            for j in range(len(label)):
                P1 = Poisson(img_path=dir_image, fps=fps)
                new_list, i = P1.detection(list_image, skip_limit = skip_limit)

                list_individus.append(P1)
                list_image = new_list + list_image[i+1:]

    list_poisson = []

    for i in list_individus:
        if i.nb_images >= min_image:
            list_poisson.append(i)



    if not list_poisson:
        print('pas de poisson dans la vidéo')

    else:

        if not save_name:
            try:
                save_name = str(dir_path)[:-6] + str(dir_path).split('/')[-2] + '.csv'
            except IndexError:
                save_name = str(dir_path)[:-6] + str(dir_path).split('\\')[-2] + '.csv'
            mode = 'w'
        else :
            mode = 'a'

        with open(save_name, mode) as f:
            writer = csv.writer(f)
            if mode == 'w':
                row = ['name', 'date',  'temps_reel', 'time_code', 'frame', 'nb_frame',
                        'espece1', 'espece2', 'espece3', 'probabilite1', 'probabilite2',
                        'probabilite3', 'direction']
                writer.writerow(row)
                poisson_path = os.path.join(str(dir_path), '..', 'Passages')
                if os.path.isdir(poisson_path):
                    shutil.rmtree(poisson_path)
                os.mkdir(poisson_path)
            for i in list_poisson:
                i.def_espece()
                i.make_name()
                i.calcul_direction()
                print(i)
                i.save_position(poisson_path)
                writer.writerow(i.out_list())

def poisson_file(path):
    color = ((0,0,255), (0,255,255),(128,128,128), (128,0,255), (128,0,128),
             (0,128,128), (128,255,0), (0,255,128), (0,128,255))
    poissons = glob.glob(path + '/*.csv')
    with open(os.path.join(path, '..', 'poissons' + '.csv'), 'w') as f:
        writer = csv.writer(f)
        for i in poissons:
            ind = pd.read_csv(i,header=None)
            col = random.choice(color)
            for j in ind.values:
                j = np.append(j,col)
                j[5] = int(j[5].split('_')[-1])
                writer.writerow(j)
                
def extract_image(image, org_label):
    
    coord_xyhw = org_label[1:5]
    x1, y1, x2, y2 = xywhn2xyxy1(coord_xyhw, image.shape[1], image.shape[0])
    image = cv2.rectangle(image,(x1,y1),(x2,y2),
                          (org_label[6],org_label[7],org_label[8]),2)      
    
    return image


def make_vid(name):
    
    poisson_file(os.path.join(name,'Passages'))

    ext = '.avi'
    video = os.path.join(name,os.path.basename(name)+ext)    
    destination = os.path.join(name,os.path.basename(name)+'_dl.avi')

    try:
        labels = pd.read_csv(os.path.join(name, 'poissons.csv'), header=None)
    except FileNotFoundError:
        return
    
    if labels.empty:
        return
    
    labels = labels.sort_values(5)
    cap = cv2.VideoCapture(video)
    if int(cap.get(5)) == 0:
        cap = cv2.VideoCapture(video[:-3]+'mp4')
    filename = destination
    x = int(cap.get(3))
    y = int(cap.get(4))
    fps = int(cap.get(5))
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(filename,fourcc, fps, (x,y)) 




    nb_frame = int(cap.get(7))
    l = 0
    labels_ind = labels.iloc[l]
    
    font = cv2.FONT_HERSHEY_SIMPLEX 
    for i in range(nb_frame):
        ret, frame = cap.read()
        text = f'{i}/{nb_frame}'
        if ret == True:
            
            frame = cv2.putText(frame, text, (10,30), font, 1, (0,255,255), 1)
            if labels_ind[5] == i:
                frame = extract_image(frame, labels_ind)
                l+=1
                try:
                    labels_ind = labels.iloc[l]
                    while i == labels_ind[5]:
                        l += 1
                        labels_ind = labels.iloc[l]
                except IndexError:
                    continue
                
            out.write(frame)

    cap.release()
    out.release()
    cv2.destroyAllWindows()
    
if __name__ == '__main__':
    # dir_vd = '/media/hersant_m/Nouveau nom/test_yolov5_VD1'
    # video_vd = os.listdir(dir_vd)
    # for i in video_vd:
    #     os.mkdir(os.path.join(dir_vd, i, 'Poisson'))
    #     analyse_1_video(os.path.join(dir_vd, i, 'labels'),skip_limit=1, min_image=4)

    analyse_1_video('/home/hersant_m/Documents/Yolo/video_aris/ARIS_2017-10-25_003000/labels',
                    skip_limit=2, min_image=5)
