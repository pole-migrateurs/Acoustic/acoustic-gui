import argparse
import time
from pathlib import Path

import cv2
import os
import shutil

import numpy as np
import pandas as pd
import datetime


import torch
import torch.backends.cudnn as cudnn
from numpy import random

from models.experimental import attempt_load
from utils.datasets import LoadStreams, LoadImages
from utils.general import check_img_size, check_requirements, check_imshow, non_max_suppression, apply_classifier, \
    scale_coords, xyxy2xywh, strip_optimizer, set_logging, save_one_box
from utils.plots import plot_one_box
from utils.torch_utils import select_device, load_classifier, time_synchronized
from utils.Class_Poisson import analyse_1_video, make_vid
import multiprocessing as mp

def folderOrganiser(pwd,endpath, vid_formats):
    """
    Function that organise the files. Starting from a folder with
    a list of files, it creates a folder per file and moves the video into it.

    Input:
    - path of the folder
    - path where the videos are saved
    - list of accepted video formats, defined in main
    """
    for r, d, f in os.walk(pwd):
        for file in f:
            for form in vid_formats:
                if file.endswith(form):

                    folder = os.path.splitext(file)[0]
                    path = os.path.join(endpath,folder)
                    start = os.path.join(r,file)
                    final = os.path.join(path,file)

                    try:
                        os.mkdir(path)
                    except FileExistsError:
                        print('Folder', folder, ' exist already!')

                    try:
                        os.rename(start,final)
                    except FileNotFoundError:
                        if os.path.isfile(final):
                            print('I\'ve already found the following video file in its right place: I do nothing!')
                            print(final)
                        else:
                            print('Something wrong! I have not found the video neither in starting or final folder!')
                            print(start)
                            print(final)
                            
    list_dir = os.listdir(pwd)
    for file in list_dir:
        if file.endswith('_header.csv'):
            shutil.move(os.path.join(pwd, file), os.path.join(pwd, file[:-11], file))
    return

def folderReOrgenaser(pwd):
    """
    delete unnecessary files at the end of the analyse
    Move base video in the parent directory
    create global csv
    
    Input:
        - path of the folder 
    """
    
    csv_global = pd.DataFrame(columns = ['name', 'date',  'temps_reel', 'time_code', 'frame', 'nb_frame',
                            'espece1', 'espece2', 'espece3', 'probabilite1', 'probabilite2',
                            'probabilite3', 'direction'])
    list_dir = os.listdir(pwd)
    for dirs in list_dir:
        try:           
            list_files = os.listdir(os.path.join(pwd, dirs))
        except NotADirectoryError:
            continue
        for file in list_files:
            if file.endswith('_FILT.mp4') or file.endswith('_obd.mp4'):
                os.remove(os.path.join(pwd, dirs, file))
            if file == dirs + '.avi' or file == dirs + '.mp4':
                shutil.move(os.path.join(pwd, dirs, file), pwd)
            if file.endswith('.csv') and not file.endswith('_header.csv'):
                csv_temp = pd.read_csv(os.path.join(pwd, dirs, file))
                csv_global = csv_global.append(csv_temp)
                
    csv_global.to_csv(os.path.join(pwd, 'csv_global.csv'), index=False)
                    
def onefoldercleaner(path):
    """
    Parameters
    ----------
    path : directory path
    
    delete FILT and obd video and Move base video in the parent directory
    for one directory

    """
    list_files = os.listdir(path)
    os.remove(os.path.join(path, 'poissons.csv')) 
    shutil.rmtree(os.path.join(path, 'Passages')) 
    for file in list_files:
        if file.endswith('_FILT.mp4') or file.endswith('_obd.mp4'):
            os.remove(os.path.join(path, file))
        if file == os.path.basename(path) + '.avi' or file == os.path.basename(path) + '.mp4':
            shutil.move(os.path.join(path, file), os.path.abspath(os.path.join(path, os.pardir)))
            

def bgkSegMain(pwd, vid_form, threads=2):
    """
    Function that call the segmentation routine, feeding the parameters specific for the camera.

    Input:
    - path of the folder
    - list of accepted video formats, defined in main

    Output:
    - list of paths of original videos
    - list of paths of filtered videos
    """

    videos = []

    for root, dirs, files in os.walk(pwd):
        for file in files:
            for form in vid_form:
                if file.endswith(form):
                    videos.append(os.path.join(root, file))

    print('Starting Segmentation')

    pool = mp.Pool(threads)
    pool.map(segmentation, videos)
    pool.close()

    # create array with FILT videos
    videos_filt = []
    for root, dirs, files in os.walk(pwd):
        for file in files:
            if file.endswith('_FILT.mp4'):
                videos_filt.append(os.path.join(root, file))
    return videos, videos_filt

def segmentation(video):
    '''
    Function that make the segmentation for one video
    
    Input:
        -one video

    '''

    vid_name = os.path.basename(video)
    camera = vid_name.split('_')[0]
    if camera == 'DIDSON':
        # threshold set to 130
        threshold = 50
        bgkSeg(video, threshold)
    elif camera == 'ARIS':
        # threshold set to 50
        threshold = 50
        bgkSeg(video, threshold)
    elif camera == 'OCULUS':
        print("The OCULUS camera is not yet implemented")
    elif camera == 'BLUEVIEW':
        print("The BLUEVIEW camera is not yet implemented")
    else:
        print("Something wrong! Check if the filenames respect the codename - I was unable to get the camera type.")

def bgkSeg(video, t):
    """
    Function that segmentates a video.

    Input:
    - path of a video
    - threshold parameter

    Ouput:
    - filtered video
    """
    print(f'start segmentation on video: {video}')
    cap = cv2.VideoCapture(video)
    fgbg = cv2.createBackgroundSubtractorMOG2(varThreshold = t)
    x = int(cap.get(3))
    y = int(cap.get(4))
    fps = int(cap.get(5))

    root, video_name = os.path.split(video)
    parse = video_name.split('.')
    video_name = parse[0] + '_FILT.mp4'
    filename = os.path.join(root, video_name)

    nb_frame = int(cap.get(7))

    if (cap.isOpened()== True):
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        out = cv2.VideoWriter(filename,fourcc, fps, (x,y),0)

        for i in range(nb_frame):
          ret, frame = cap.read()
          if ret == True:
             fgmask = fgbg.apply(frame)
             out.write(fgmask)


        cap.release()
        out.release()
        cv2.destroyAllWindows()
    else:
        print("Error with video stream for", video)
    return


def denoiser(listOfVids, threads=2):
    """
    This function does 2 operations:
    1) it denoise a list of filtered videos
    2) it compose the final videos, in which the images BGR are, in order,
    B = original image
    G = filtered image
    R = denoised image

    Input:
    - A list of paths of videos. For n videos, such list has the dimension 2 x n (it is a list of lists)
    since for each video the function needs also the path of the filtered video

    Output:
    - A list of paths of videos ready to be feed into the DL architecture
    """

    print('Starting Denoiser')

    pool = mp.Pool(threads)
    listForDL = pool.map(denoise_1_vid, listOfVids)
    pool.close()

    return listForDL

def denoise_1_vid(video):

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    cap_orig = cv2.VideoCapture(video[0])
    cap_filt = cv2.VideoCapture(video[1])
    print('Starting denoising on video:', video[0])

    vid_name = video[1].replace('FILT','obd')
    fps = int(cap_orig.get(5))
    size = (int(cap_orig.get(3)),int(cap_orig.get(4)))
    out = cv2.VideoWriter(vid_name,cv2.VideoWriter_fourcc(*'mp4v'), fps, size)

    nb_frame = int(cap_orig.get(7))

    for j in range(nb_frame):

        ret_o, frame_o = cap_orig.read()
        ret_f, frame_f = cap_filt.read()

        if ret_o and ret_f:
           h, w, c = frame_f.shape
           size = (w,h)
           dst = cv2.medianBlur(frame_f,3)

           opening = cv2.morphologyEx(dst, cv2.MORPH_OPEN, kernel)

           img_multi = np.zeros((h, w, 3), dtype=np.uint8)

           img_multi[:,:,0] = frame_o[:,:,0]
           img_multi[:,:,1] = frame_f[:,:,1]
           img_multi[:,:,2] = opening[:,:,2]

           img_multi = drawAxis(img_multi)
           out.write(img_multi)

    out.release()
    return vid_name

def drawAxis(img):
    """
    This function draws a green axis in pixels on the final images.
    This is done to rely the bounding boxes coordinates to the image.

    Input:
    - an image

    Output:
    - the same image with axis
    """
    ny, nx, nz = img.shape
    start = (0,ny)
    end_x = (nx,ny)
    end_y = (0,0)

    cv2.line(img,start,end_x,(0,255,0),5)
    cv2.line(img,start,end_y,(0,255,0),5)

    lenght=100
    tot_xticks = int(nx/lenght)
    tot_yticks = int(ny/lenght)

    for i in range(tot_xticks+1):
        tick_x_start = (lenght*i,ny)
        tick_x_end = (lenght*i,ny-10)
        cv2.line(img,tick_x_start,tick_x_end,(0,255,0),5)
        text_pos = (lenght*i-15,ny-15)
        cv2.putText(img,str(lenght*i),text_pos,cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,255,0),2)

    for i in range(tot_yticks+1):
        tick_y_start = (0,lenght*i)
        tick_y_end = (10,lenght*i)
        cv2.line(img,tick_y_start,tick_y_end,(0,255,0),5)
        text_pos = (15,lenght*i)
        cv2.putText(img,str(lenght*i),text_pos,cv2.FONT_HERSHEY_SIMPLEX,0.5,(0,255,0),2)

    return img

#########################################
# START OF THE DEEP LEARNING ARCHITECTURE
#########################################

def detect(opt):
    source, weights, view_img, save_txt, imgsz = opt.source, opt.weights, opt.view_img, opt.save_txt, opt.img_size
    save_img = not opt.nosave and not source.endswith('.txt')  # save inference images
    webcam = source.isnumeric() or source.endswith('.txt') or source.lower().startswith(
        ('rtsp://', 'rtmp://', 'http://', 'https://'))

    # Directories
    #save_dir = increment_path(Path(opt.project) / opt.name, exist_ok=opt.exist_ok)  # increment run
    k =  Path(opt.source)
    #save_dir = (k.parent / opt.name)
    save_dir = k.parent
    (save_dir / 'labels' if save_txt else save_dir).mkdir(parents=True, exist_ok=True)  # make dir

    # Initialize
    set_logging()
    device = select_device(opt.device)
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    model = attempt_load(weights, map_location=device)  # load FP32 model
    stride = int(model.stride.max())  # model stride
    imgsz = check_img_size(imgsz, s=stride)  # check img_size
    if half:
        model.half()  # to FP16

    # Second-stage classifier
    classify = False
    if classify:
        modelc = load_classifier(name='resnet101', n=2)  # initialize
        modelc.load_state_dict(torch.load('weights/resnet101.pt', map_location=device)['model']).to(device).eval()

    # Set Dataloader
    vid_path, vid_writer = None, None
    if webcam:
        view_img = check_imshow()
        cudnn.benchmark = True  # set True to speed up constant image size inference
        dataset = LoadStreams(source, img_size=imgsz, stride=stride)
    else:
        dataset = LoadImages(source, img_size=imgsz, stride=stride)

    # Get names and colors
    names = model.module.names if hasattr(model, 'module') else model.names
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in names]

    # Run inference
    if device.type != 'cpu':
        model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(next(model.parameters())))  # run once
    t0 = time.time()
    for path, img, im0s, vid_cap in dataset:
        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # Inference
        t1 = time_synchronized()
        pred = model(img, augment=opt.augment)[0]

        # Apply NMS
        pred = non_max_suppression(pred, opt.conf_thres, opt.iou_thres, classes=opt.classes, agnostic=opt.agnostic_nms)
        t2 = time_synchronized()

        # Apply Classifier
        if classify:
            pred = apply_classifier(pred, modelc, img, im0s)

        # Process detections
        for i, det in enumerate(pred):  # detections per image
            if webcam:  # batch_size >= 1
                p, s, im0, frame = path[i], '%g: ' % i, im0s[i].copy(), dataset.count
            else:
                p, s, im0, frame = path, '', im0s.copy(), getattr(dataset, 'frame', 0)

            p = Path(p)  # to Path
            save_path = str(save_dir / p.name)  # img.jpg
            save_path = save_path.replace('.mp4','_DL.mp4')
            txt_path = str(save_dir / 'labels' / p.stem) + ('' if dataset.mode == 'image' else f'_{frame}')  # img.txt
            s += '%gx%g ' % img.shape[2:]  # print string
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
            if len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string

                # Write results
                for *xyxy, conf, cls in reversed(det):
                    if save_txt:  # Write to file
                        xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                        line = (cls, *xywh, conf) if opt.save_conf else (cls, *xywh)  # label format
                        with open(txt_path + '.txt', 'a') as f:
                            f.write(('%g ' * len(line)).rstrip() % line + '\n')

                    if save_img or opt.save_crop or view_img:  # Add bbox to image
                        c = int(cls)  # integer class
                        label = None if opt.hide_labels else (names[c] if opt.hide_conf else f'{names[c]} {conf:.2f}')

                        plot_one_box(xyxy, im0, label=label, color=colors[c], line_thickness=opt.line_thickness)
                        if opt.save_crop:
                            save_one_box(xyxy, im0s, file=save_dir / 'crops' / names[c] / f'{p.stem}.jpg', BGR=True)

            # Print time (inference + NMS)
            print(f'{s}Done. ({t2 - t1:.3f}s)')

            # Stream results
            if view_img:
                cv2.imshow(str(p), im0)
                cv2.waitKey(1)  # 1 millisecond

            # Save results (image with detections)
            if save_img:
                if dataset.mode == 'image':
                    cv2.imwrite(save_path, im0)
                else:  # 'video' or 'stream'
                    if vid_path != save_path:  # new video
                        vid_path = save_path
                        if isinstance(vid_writer, cv2.VideoWriter):
                            vid_writer.release()  # release previous video writer
                        if vid_cap:  # video
                            fps = vid_cap.get(cv2.CAP_PROP_FPS)
                            w = int(vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                            h = int(vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                        else:  # stream
                            fps, w, h = 30, im0.shape[1], im0.shape[0]
                            save_path += '.mp4'
                        vid_writer = cv2.VideoWriter(save_path, cv2.VideoWriter_fourcc(*'mp4v'), fps, (w, h))
                    vid_writer.write(im0)

            if vid_cap:
                fps = vid_cap.get(cv2.CAP_PROP_FPS)

    if save_txt or save_img:
        s = f"\n{len(list(save_dir.glob('labels/*.txt')))} labels saved to {save_dir / 'labels'}" if save_txt else ''
        print(f"Results saved to {save_dir}{s}")

    # Save a csv with all the detection in the video
    if save_txt:
        analyse_1_video(save_dir / 'labels',
                        video=True,
                        skip_limit=opt.skip_limit,
                        min_image=opt.min_image,
                        fps=fps)
    make_vid(save_dir)                    
    onefoldercleaner(save_dir)

    print(f'Done. ({time.time() - t0:.3f}s)')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--weights', nargs='+', type=str, default='yolov5s.pt', help='model.pt path(s)')
    parser.add_argument('--source', type=str, default='data/images', help='source')  # file/folder, 0 for webcam
    parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    parser.add_argument('--view-img', action='store_true', help='display results')
    parser.add_argument('--save-txt',default=True, action='store_true', help='save results to *.txt')
    parser.add_argument('--save-conf', action='store_true', help='save confidences in --save-txt labels')
    parser.add_argument('--save-crop', action='store_true', help='save cropped prediction boxes')
    parser.add_argument('--nosave', action='store_true', help='do not save images/videos')
    parser.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, or --class 0 2 3')
    parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    parser.add_argument('--augment', action='store_true', help='augmented inference')
    parser.add_argument('--update', action='store_true', help='update all models')
    parser.add_argument('--project', default='runs/detect', help='save results to project/name')
    parser.add_argument('--name', default='exp', help='save results to project/name')
    parser.add_argument('--exist-ok', action='store_true', help='existing project/name ok, do not increment')
    parser.add_argument('--line-thickness', default=3, type=int, help='bounding box thickness (pixels)')
    parser.add_argument('--hide-labels', default=False, action='store_true', help='hide labels')
    parser.add_argument('--hide-conf', default=False, action='store_true', help='hide confidences')
    parser.add_argument('--save-path',type=str, default='data/images', help='path to save files different from original path')

    parser.add_argument('--iou-thres', type=float, default=0.45, help='IOU threshold for NMS')                              # >=0 x <=1     confiance IOU thres
    parser.add_argument('--conf-thres', type=float, default=0.25, help='object confidence threshold')                       # >=0 x <=1     confiance minimum de détéction
    parser.add_argument('--skip_limit', default=3, type=int, help='max images between detection for the same fish')         # >=1 x <=5     Nombre d'image vide entre les détéctions
    parser.add_argument('--min_image', default=5, type=int, help='min image to keep a fish')                                # >=2 x <=10    Nombre minimum d'image détécté pour un poisson
    parser.add_argument('--maxCPU', default=2, type=int, help='number of threads for preprocessing')

    opt = parser.parse_args()

    start_time = time.time()

    print('Parameters for the analysis:')
    print(opt)
    check_requirements(exclude=('pycocotools', 'thop'))

    vid_formats = ['.avi', '.mp4']
    home = opt.source
    save_path = opt.save_path
    if save_path == 'data/images':
        save_path = home

    folderOrganiser(home,save_path,vid_formats)
    print('Done file organising')

    listOfOrig, listOfFilt = bgkSegMain(save_path, vid_formats, threads=opt.maxCPU)
    print('Done background segmentation')
    listOfVids = []
    for i in range(len(listOfOrig)):
        listOfVids.append([listOfOrig[i],listOfFilt[i]])
    print('Done array creation')
    listForYolo = denoiser(listOfVids, threads=opt.maxCPU)
    print('Done denoising')
    
    ptr_time = time.time()
    pretr_time = str(datetime.timedelta(seconds=ptr_time - start_time))
    

    with torch.no_grad():
        if opt.update:  # update all models (to fix SourceChangeWarning)
            for opt.weights in ['yolov5s.pt', 'yolov5m.pt', 'yolov5l.pt', 'yolov5x.pt']:
                detect(opt=opt)
                strip_optimizer(opt.weights)
        else:
            for vidpath in listForYolo:
                opt.source = vidpath
                detect(opt=opt)
          
     
    folderReOrgenaser(save_path)
    end_time = time.time()
    total_time = str(datetime.timedelta(seconds=end_time - ptr_time))  


    with open(os.path.join(save_path, 'log.txt'), 'a') as f:
        f.write(f'{datetime.datetime.today()} ; Nombre de CPU : {opt.maxCPU} ; pre-processing run time for {len(listForYolo)} videos : {pretr_time} \n')
        f.write(f'{datetime.datetime.today()} ; device : {select_device(opt.device)} ; analyse run time for {len(listForYolo)} videos : {total_time} \n')
        f.write('\n')
    
