from sonarisdidson.sondidson import Sondidson
from sonarisdidson.sonaris import Sonaris
from pathos.multiprocessing import ProcessingPool as Pool
import os
import argparse
import time
import datetime
import shutil


def run(video_list):
    video_list.convert()
    
    name = video_list.avi_file[:-4]   
    with open(f'{name}_header.csv', 'w') as f:
        f.write('File_header \n')
        file_header = video_list.file_header
        for key in file_header.keys():
            f.write(f'{key};{file_header[key]}\n')                        
        f.write('\n Frame_header \n')    
        frame_header = video_list.frame_header
        for key in frame_header.keys():
            f.write(f'{key};{frame_header[key]}\n')  
    # print(video_list.__dict__)
        
def listing(pwd):
    listofAris = []
    listofArisConv = []
    listofDdf = []
    listofDdfConv = []

    for r, d, f in os.walk(pwd):
        for file in f:
            if file.endswith('.aris'):
                if not file.startswith('ARIS_'):
                    shutil.move(os.path.join(r, file), os.path.join(r, 'ARIS_' + file))
                    file = 'ARIS_' + file
                fileconv = file.split('.')[0] + '.avi'
                listofAris.append(os.path.join(r, file))
                listofArisConv.append(os.path.join(r, fileconv))
            elif file.endswith('.ddf'):
                if not file.startswith('DIDSON_'):
                    shutil.move(os.path.join(r, file), os.path.join(r, 'DIDSON_' + file))
                    file = 'DIDSON_' + file
                fileconv = file.split('.')[0] + '.avi'
                listofDdf.append(os.path.join(r, file))
                listofDdfConv.append(os.path.join(r, fileconv))

    return listofAris,listofArisConv,listofDdf,listofDdfConv

def converter(listofAris,listofArisConv,listofDdf,listofDdfConv,maxCPU):

    tab=[]
    for i in range(0,len(listofAris),maxCPU):
        chunk_orig = listofAris[i:i+maxCPU]
        chunk_conv = listofArisConv[i:i+maxCPU]
        print(chunk_orig)
        for j in range(len(chunk_orig)):
            tab.append(Sonaris(chunk_orig[j],chunk_conv[j]))
        pool = Pool(nodes=maxCPU)
        pool.map(run, tab)
        pool.close()
        pool.join()
        pool.clear()
        tab=[]

    for i in range(0,len(listofDdf),maxCPU):
        chunk_orig = listofDdf[i:i+maxCPU]
        chunk_conv = listofDdfConv[i:i+maxCPU]
        print(chunk_orig)
        for j in range(len(chunk_orig)):
            tab.append(Sondidson(chunk_orig[j],chunk_conv[j]))
            
        pool = Pool(nodes=maxCPU)
        pool.map(run, tab)
        pool.close()
        pool.join()
        pool.clear()
        tab=[]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, default='./', help='path of the videos')
    parser.add_argument('--maxCPU', type=str, default='1', help='max CPU top be used, to use all CPU give the keyword "all"')
    opt = parser.parse_args()
    
    start_time = time.time()
    
    path = opt.path
    if opt.maxCPU == 'all':
        maxCPU=len(os.sched_getaffinity(0))
    else:
        maxCPU = int(opt.maxCPU)

    listofAris,listofArisConv,listofDdf,listofDdfConv = listing(path)
    converter(listofAris,listofArisConv,listofDdf,listofDdfConv,maxCPU)
    
    end_time = time.time()
    total_time = str(datetime.timedelta(seconds=end_time - start_time))

    with open(os.path.join(path, 'log.txt'), 'a') as f:
        f.write(f'{datetime.datetime.today()} ; Nombre de CPU : {opt.maxCPU} ; conversion run time for {len(listofAris)+len(listofDdf)} videos : {total_time} \n')