# Acoustic GUI

This project is a Java GUI used to run a Python fish finder program

## Requirements for Windows, Linux and macOS
### Requirement java
 - Java version >= 8
If you don't have java you can install it here : https://www.java.com/fr/
### Requirement python
- Python 3.7
If you don't have python you can install it here : https://www.python.org/downloads/release/python-379/
- Pip (usually pip is installed at the same time as python)

## How to Install the project ?
Recover the project then launch "Install.jar" :
```
git clone https://forgemia.inra.fr/pole-migrateurs/Acoustic/acoustic-gui.git
cd acoustic-gui/Fish/
```
To launch the Install.jar, you can run ```java -jar Install.jar``` or double click on it in your files.
Once done, you should see a "poissonEnv" folder appear at the same tree level as where you are.

## How to launch the project ?
Change to the folder where the fish.jar file is located. There must be in this same folder the directory "pythonCode" and "PoissonEnv"
To launch the project, you can run ```java -jar poisson.jar```or double click on it in your files.

## How to use the GUI ?
You can find out how the interface works via the Manual here : https://forgemia.inra.fr/pole-migrateurs/Acoustic/acoustic-gui/Manuel.docx

## How to modify the interface ?
###	Import the code into IntelliJ
If you do not have Intellij you can download it via the following link : https://www.jetbrains.com/fr-fr/idea/download
- Open Intellij then go to "File" "New" and "Project"
- Click on "Next"
- Check "Create project from template"
- Click on "Next"
- Name the project
- Click on "Finish"
- Copy the code in the fish folder that you extracted previously
- Paste the code in your Intellij project folder

You now have the project code in Intellij.

### Setting up of the artefact
- Go to "File" then click on "Project Structure"
- Click on "Artifact"
- Click on the "+"
- Go to "JAR" then click on "From modules with dependencies"
- Click on the folder icon
- Click on "Main" then "OK"
- Click on "OK"
- Click on "OK"

the artefact is ready

### Creation of the jar
- Go to "Build" then click on "Build Artifact"
- Click on "Build"
The created jar can be found in fish\out\artifacts\fish_jar
To launch it, just double click on it. Do not forget to move it to put it at the same level as the "pythonCode" folder so that the analysis can be started correctly.a

I have explained in detail in the Manual how to modify the code with screenshots.

## Code description
To code this project I used the MVC model (Models Views Controllers)
In the controllers folder are the classes used to respond to button actions
In the models folder, is the "base" class which is used to display all the pages of the application
In the views folder, are the classes used to create the different pages of the application

I made a class diagram for the interface which shows all the relationships between the classes. The diagram is available here : https://forgemia.inra.fr/pole-migrateurs/Acoustic/acoustic-gui/poisson.svg

## Modification du code en python
Il est possible de modifier le code en python il faut juste faire attention à ne pas modifier l'arborecence et le nom des fichiers pipeline_YOLOv5_NOfilter_v3.py pour l'analyse et sonar_converter.py pour la convertion des vidéos.

### Convertion
Le code permettant de lancer la convertion des vidéos se trouve dans la classe ```base.java```, commence de la ligne 150 et ce fini à la ligne 190. Les lignes précises où sont lancé le code python sont ```161``` et ```176```.

### Analyse
Le code permettant de lancer la convertion des vidéos se trouve dans la classe ```AAnalyse.java```, commence de la ligne 744 et ce fini à la ligne 1044. Les lignes précises où sont lancé le code python sont ```761```, ```796```, ```832```, ```868```, ```907```, ```941```, ```978``` et ```1011```.
